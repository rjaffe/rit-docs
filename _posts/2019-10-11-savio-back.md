---
title:  "Savio back online"
categories: hpc
permalink: test-post-from-last-year.html
tags: [news]
permalink: services/high-performance-computing/status-and-announcements/savio-back
sidebar: hpc_sidebar
---

Earlier this afternoon, we received word that the non-UPS power had been restored to the Warren Hall datacenter so we immediately started work to bring Savio back online. At this point, Savio is back in full production so users can return to doing their work. We did notice a few jobs were still running when we brought the system down before the power outage. Users should check their last running jobs and restart if necessary.

{% include links.html %}
