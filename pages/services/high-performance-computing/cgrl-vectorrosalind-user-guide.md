---
title: CGRL (Vector/Rosalind) User Guide
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/cgrl-vectorrosalind-user-guide

---

<p>This is the user guide for <a href="http://qb3.berkeley.edu/cgrl/"> Computational Genomics Resource Laboratory (CGRL)</a> users. The CGRL provides access to two computing clusters collocated within the larger Savio system administered by <a href="http://research-it.berkeley.edu/programs/berkeley-research-computing">Berkeley Research Computing</a> at the University of California, Berkeley. Vector is a heterogeneous cluster which is accessed through the Savio login nodes, but is independent from the rest of Savio and exclusively used by the CGRL. Rosalind is a condominium of identical nodes within Savio. Through the <a href="https://research-it.berkeley.edu/services/high-performance-computing/condo-partner-access">condo model of access</a>, CGRL users can utilize a number of Savio nodes equal to those contributed by Rosalind.</p>
<h4 id="Account-Requests"><span style="font-size: 1.2em;">Account Requests</span></h4>
<p>You can request new accounts through the <a href="http://qb3.berkeley.edu/cgrl/">Computational Genomics Resource Laboratory (CGRL)</a> by filling out this<a href="http://qb3.berkeley.edu/cgrl/wp-content/uploads/2016/06/CGRLApplication.pdf"> form</a> and emailing it to <a href="mailto:cgrl@berkeley.edu">cgrl@berkeley.edu</a>.</p>
<h4 id="Logging-In"><span style="font-size: 1.2em;">Logging in</span></h4>
<p>Vector and Rosalind (Savio)&nbsp;use One Time Passwords (OTPs) for login authentication. For details, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/logging-savio">Logging into BRC</a>.</p>
<p>Use SSH to log into:&nbsp; <code>hpc.brc.berkeley.edu</code></p>
<h4 id="Transferring-Data"><span style="font-size: 1.2em;">Transferring Data</span></h4>
<p>To transfer data to/from or between Vector and Rosalind (Savio), use the dedicated Data Transfer Node (DTN):&nbsp; <code>dtn.brc.berkeley.edu</code></p>
<p>If you're using <a href="http://research-it.berkeley.edu/services/high-performance-computing/using-globus-connect-savio">Globus</a> to transfer files, the Globus endpoint is:&nbsp; <code>ucb#brc</code></p>
<p>For details about how to transfer files to and from the cluster, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/transferring-data">Transferring Data</a>.</p>
<h4 id="Storage-and-Backup"><span style="font-size: 1.2em;">Storage and Backup</span></h4>
<p>The following storage systems are available to&nbsp;CGRL users. For running jobs, compute nodes within a cluster can only directly access the storage as listed below. The DTN can be used to transfer data between the locations accessible to only one cluster or the other, as detailed in the previous section.</p>
<table cellspacing="0" border="1" align="center"><tbody><tr style="background-color: rgb(211, 211, 211);"><th>Name</th>
<th>Cluster</th>
<th>Location</th>
<th>Quota</th>
<th>Backup</th>
<th>Allocation</th>
<th>Description</th>
</tr><tr><td>Home</td>
<td>Both</td>
<td><code>/global/home/users/$USER</code></td>
<td>10 GB</td>
<td><strong>Yes</strong></td>
<td>Per User</td>
<td>Home directory ($HOME) for permanent data</td>
</tr><tr><td>Scratch</td>
<td rowspan="2">Vector</td>
<td><code>/</code>clusterfs/vector/scratch<code>/$USER</code></td>
<td>none</td>
<td>No</td>
<td>Per User</td>
<td>Short-term, large-scale storage for computing</td>
</tr><tr><td>Group</td>
<td>/clusterfs/vector/instrumentData/</td>
<td>300 GB</td>
<td>No</td>
<td>Per Group</td>
<td>Group-shared storage for computing</td>
</tr><tr><td>Scratch</td>
<td rowspan="3">Rosalind (Savio)</td>
<td><code>/global/scratch/$USER</code></td>
<td>none</td>
<td>No</td>
<td>Per User</td>
<td>Short-term, large-scale Lustre storage for very high-performance computing</td>
</tr><tr><td>Condo User</td>
<td>/clusterfs/rosalind/users/<code>$USER</code></td>
<td>none</td>
<td>No</td>
<td>Per User</td>
<td>Long-term, large-scale user storage</td>
</tr><tr><td>Condo Group</td>
<td>/clusterfs/rosalind/groups/</td>
<td>none</td>
<td>No</td>
<td>Per Group</td>
<td>Long-term, large-scale group-shared storage</td>
</tr></tbody></table><h4 id="Hardware-Configuration"><span style="font-size: 1.2em;">Hardware Configuration</span></h4>
<p>Vector and Rosalind are heterogeneous, with a mix of several different types of nodes. Please be aware of these various hardware configurations, along with their associated <a href="#Scheduler-Configuration" class="toc-filter-processed">scheduler configurations</a>, when specifying options for running your jobs.</p>
<table border="1" align="center"><tbody><tr style="background-color:#D3D3D3"><th>Cluster</th>
<th>Nodes</th>
<th>Node List</th>
<th>CPU</th>
<th>Cores/Node</th>
<th>Memory/Node</th>
<th>Scheduler Allocation</th>
</tr><tr><td rowspan="4">Vector</td>
<td rowspan="4">11</td>
<td>n00[00-03].vector0</td>
<td>Intel Xeon X5650, 2.66 GHz</td>
<td>12</td>
<td>96 GB</td>
<td>By Core</td>
</tr><tr><td>n0004.vector0</td>
<td>AMD Opteron 6176, 2.3 GHz</td>
<td>48</td>
<td>256 GB</td>
<td>By Core</td>
</tr><tr><td>n00[05-08].vector0</td>
<td>Intel Xeon E5-2670, 2.60 GHz</td>
<td>16</td>
<td>128 GB</td>
<td>By Core</td>
</tr><tr><td>n00[09]-n00[10].vector0</td>
<td>Intel Xeon X5650, 2.66 GHz</td>
<td>12</td>
<td>48 GB</td>
<td>By Core</td>
</tr><tr><td>Rosalind (Savio1)</td>
<td>8</td>
<td>floating condo within:&nbsp;&nbsp;&nbsp;&nbsp;
<p>n0[000-095].savio1, n0[100-167].savio1</p></td>
<td>Intel Xeon E5-2670 v2, 2.50 GHz</td>
<td>20</td>
<td>64 GB</td>
<td>By Node</td>
</tr><tr><td>Rosalind (Savio2 HTC)</td>
<td>8</td>
<td>floating condo within:&nbsp;&nbsp;&nbsp;&nbsp;
<p>n0[000-011].savio2, n0[215-222].savio2</p></td>
<td>Intel Xeon E5-2643 v3, 3.40 GHz</td>
<td>12</td>
<td>128 GB</td>
<td>By Core</td>
</tr></tbody></table><h4 id="Scheduler-Configuration"><span style="font-size: 1.2em;">Scheduler Configuration</span></h4>
<p>The clusters uses the SLURM scheduler to manage jobs. When submitting your jobs via <code>sbatch</code> or <code>srun</code> commands, use the following SLURM options:</p>
<p><strong>NOTE:</strong> To check which QoS you are allowed to use, simply run "sacctmgr -p show associations user=$USER"</p>
<table border="1" align="center"><tbody><tr style="background-color:#D3D3D3"><th>Partition</th>
<th>Account</th>
<th>Nodes</th>
<th>Node List</th>
<th>Node Feature</th>
<th>QoS</th>
<th>QoS Limit</th>
</tr><tr><td rowspan="4">vector</td>
<td rowspan="4">&nbsp;</td>
<td rowspan="4">11</td>
<td>n00[00-03].vector0</td>
<td>vector,vector_c12,vector_m96</td>
<td rowspan="4">vector_batch</td>
<td rowspan="4">48 cores max per job&nbsp;&nbsp;&nbsp;&nbsp;
<p>96 cores max per user</p></td>
</tr><tr><td>n0004.vector0</td>
<td>vector,vector_c48,vector_m256</td>
</tr><tr><td>n00[05-08].vector0</td>
<td>vector,vector_c16,vector_m128</td>
</tr><tr><td>n00[09]-n00[10].vector0</td>
<td>vector,vector_c12,vector_m48</td>
</tr><tr><td>savio</td>
<td>co_rosalind</td>
<td>8</td>
<td>n0[000-095].savio1, n0[100-167].savio1</td>
<td>savio</td>
<td>rosalind_savio_normal</td>
<td>8 nodes max per group</td>
</tr><tr><td>savio2_htc</td>
<td>co_rosalind</td>
<td>8</td>
<td>n0[000-011].savio2, n0[215-222].savio2</td>
<td>savio2_htc</td>
<td>rosalind_htc2_normal</td>
<td>8 nodes max per group</td>
</tr></tbody></table><ul><li>The settings for a job in Vector (Note: you don't need to set the "account"): <code>--partition=vector --qos=vector_batch</code></li>
<li>The settings for a job in Rosalind (Savio1): <code>--partition=savio </code>--account=co_rosalind<code> --qos=</code>rosalind_savio_normal</li>
<li>The settings for a job in Rosalind (Savio2 HTC): <code>--partition=savio2_htc </code>--account=co_rosalind<code> --qos=</code>rosalind_htc2_normal</li>
</ul><h4 id="Low-Priority-Jobs"><span style="font-size: 1.2em;">Low Priority Jobs</span></h4>
<p>As a condo contributor, are entitled to use the extra resource that is available on the SAVIO cluster (across all partitions). This is done through a low priority QoS "savio_lowprio" and your account is automatically subscribed to this QoS during the account creation stage. You do not need to request for it explicitly. By using this QoS you are no longer limited by your condo size. What this means to users is that you will now have access to the broader compute resource which is limited by the size of partitions. However this QoS does not get a priority as high as the general QoSs, such as "savio_normal" and "savio_debug", or all the condo QoSs, and it is subject to preemption when all the other QoSs become busy. Thus it has two implications:</p>
<ol><li>When system is busy, any job that is submitted with this QoS will be pending and yield to other jobs with higher priorities.</li>
<li>When system is busy and there are higher priority jobs pending, scheduler will preempt jobs that are running with this lower priority QoS. Preempted jobs can choose whether the job should be simply killed, or be automatically requeued after it's killed, at submission time. Please note that, since preemption could happen at any time, it would be very beneficial if your job is capable of checkpointing/restarting by itself, when you choose to requeue the job. Otherwise, you may need to verify data integrity manually before you want to run the job again.</li>
</ol><h4 id="Job-Script-Examples"><span style="font-size: 1.2em;">Job Script Examples</span></h4>
<p>For many examples of job script files that you can adapt and use for running your own jobs, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/running-your-jobs">Running Your Jobs</a>.</p>
<h4 id="Software-Configuration"><span style="font-size: 1.2em;">Software Configuration</span></h4>
<p>For details about how to find and access the software provided on the cluster, as well as on how to install your own, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/accessing-and-installing-software">Accessing and Installing Software</a>. CGRL users have access to the CGRL module farm of bioinformatics software (/clusterfs/vector/home/groups/software/sl-7.x86_64/modfiles), as well as the other module farms on Savio.</p>
<h4 id="Getting-Help"><span style="font-size: 1.2em;">Getting Help</span></h4>
<p>For inquiries or service requests regarding the cluster systems, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/getting-help">BRC's Getting Help page</a> or send email to <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>.</p>
<p>For questions about new accounts or installing new biology software please contact the <a href="http://qb3.berkeley.edu/cgrl/">Computational Genomics Resource Laboratory (CGRL)</a> by emailing&nbsp;<a href="mailto:cgrl@berkeley.edu">cgrl@berkeley.edu</a>.</p>
<p>&nbsp;</p>
