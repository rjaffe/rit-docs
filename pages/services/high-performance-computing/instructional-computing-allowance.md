---
title: Instructional Computing Allowance
keywords: 
last_updated: October 1, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/instructional-computing-allowance
folder: hpc
---

<p>Instructional Computing Allowances (ICAs) provide a no-cost allocation on the Savio high-performance computing cluster to instructors who need significant computational resources for their courses.</p>
<p>Each Instructional Computing Allowance is based upon a partnership agreement between BRC and the instructor. The agreement specifies a designated Point of Contact (POC) for the course (usually a GSI or staff member) who is familiar with high performance computing in Savio, and who provides the students with basic support for using Savio resources. BRC staff work with the POC to provision course-specific software and data in the Savio environment. This model provides Savio access to students learning computationally-intensive research techniques and ensures that BRC support staff are not overwhelmed.</p>
<p>New tools are under development that will support the distribution of an Instructional Computing Allowance allocation among the students in a course.</p>
<p>To apply for an allocation, please <a href="https://docs.google.com/forms/d/e/1FAIpQLSeNO3sw3YSPYarL70Bd74kz6Wl3rwyXFFtUVU08zryfMSJh_A/viewform">fill out the ICA request form</a>. If you have any questions, please contact us at <a href="mailto:brc@berkeley.edu">brc@berkeley.edu</a>.</p>
