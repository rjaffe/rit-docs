---
title: Running Your Jobs
keywords: high performance computing, running jobs
last_updated: September 30, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/running-your-jobs
folder: hpc
---

<h1 id="Overview">Overview</h1>

To submit and run jobs, cancel jobs, and check the status of jobs on the Savio cluster, you'll use the Simple Linux Utility for Resource Management (SLURM), an open-source resource manager and job scheduling system. (SLURM manages jobs, job steps, nodes, partitions (groups of nodes), and other entities on the cluster.)

There are several basic SLURM commands you'll likely use often:

<ul>
	<li><code>sbatch</code>&nbsp;- Submit a job to the batch queue system, e.g., <code>sbatch myjob.sh</code>, where <code>myjob.sh</code>&nbsp;is a SLURM job script. (On this page, you can find both a simple, <a href="#Basic-job-submission">introductory example of a job script</a>, as well as <a href="#Job-submission-with-specific-resource-requirements">many other examples of job scripts</a> for specific types of jobs you might run. Adapting and modifying one of these examples is the quickest way to get started with running batch jobs.)</li>
	<li><code>srun</code>&nbsp;- Submit an interactive job to the batch queue system</li>
	<li><code>scancel</code><code>&nbsp;</code>- Cancel a job, e.g., <code>scancel 123</code>, where 123&nbsp;is a job ID</li>
	<li><code>squeue</code>&nbsp;- Check the current jobs in the batch queue system, e.g., <code>squeue -u $USER</code> to view your own jobs</li>
	<li><code>sinfo</code>&nbsp;- View the status of the cluster's compute nodes, including how many nodes - of what types - are currently available for running jobs.</li>
</ul>

<h1 id="Charges">Charges for running jobs</h1>

When running your SLURM batch or interactive jobs on the Savio cluster under a <a href="http://research-it.berkeley.edu/services/high-performance-computing/faculty-computing-allowance">Faculty Computing Allowance</a> account (i.e. a scheduler account whose name begins with <code>fc_</code>), your usage of computational time is tracked (in effect, "charged" for, although no costs are incurred) via abstract measurement units called "Service Units." (Please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/service-units-savio">Service Units on Savio</a> for a description of how this usage is calculated.) When all of the Service Units provided under an Allowance have been <a href="http://research-it.berkeley.edu/services/high-performance-computing/options-when-faculty-computing-allowance-exhausted">exhausted</a>, no more jobs can be run under that account. To check your usage or total usage under an FCA, please use our <a href="https://research-it.berkeley.edu/services/high-performance-computing/frequently-asked-questions#q-how-can-i-check-on-my-faculty-computing-allowance-fca-usage-">check_usage.sh script</a>.

Please also note that, when running jobs on many of Savio's pools of compute nodes, you are provided with exclusive access to those nodes, and thus are "charged" for using all of that node's cores. For example, if you run a job for one hour on a standard 24-core compute node on the savio2 partition, your job will always be charged for using 24 core hours, even if it requires just a single core or a few cores. (For more details, including information on ways you can most efficiently use your computational time on the cluster, please see the <a href="http://research-it.berkeley.edu/services/high-performance-computing/service-units-savio#Scheduling">Scheduling Nodes v. Cores</a> section of <a href="http://research-it.berkeley.edu/services/high-performance-computing/service-units-savio">Service Units on Savio</a>.)

Usage tracking does not affect jobs run under a <a href="http://research-it.berkeley.edu/services/high-performance-computing/condo-cluster-service">Condo</a> account (i.e. a scheduler account whose name begins with <code>co_</code>), which has no Service Unit-based limits.

<h1 id="Key-options">Key options to set when submitting your jobs</h1>

When submitting a job, the three key options required are the <strong>account</strong>&nbsp;you are submitting under, the <strong>partition</strong>&nbsp;you are submitting to, and a maximum <strong>time</strong> limit for your job. Under some circumstances, a <strong>Quality of Service&nbsp;(QoS)</strong> is also expected, along with the partition.

<ul>
	<li><strong>Account</strong>: Each job must be submitted as part of an account, which determines which resources you have access to and how your use will be charged. Note that this account name, which you will use in your SLURM job script files, is different from your Linux account name on the cluster. For instance, for a hypothetical example user <code>lee</code>&nbsp;who has access to&nbsp;both&nbsp;the <code>physics</code>&nbsp;condo and to a Faculty Computing Allowance, their available accounts for running jobs on the cluster might be named <code>co_physics</code> and&nbsp;<code>fc_lee</code>, respectively. (See below for a command that you can run to find out what account name(s) you can use in your own job script files.)</li>
	<li><strong>Partition</strong>: Each job must be submitted to a particular partition. A partition can also be considered to be a job queue that comes with a set of constraints, such as job size limit, time limit, etc. Jobs submitted within a partition will be allocated to that partition's set of compute nodes based on the scheduling policy, until all resources within that partition are exhausted. Currently, on the Savio cluster, partitions are also associated with specific types of computational resources on which you can request your job be run, such as older or newer generations of standard compute nodes, "big memory" nodes, nodes with Graphics Processing Units (GPUs), etc. (See below for a command that you can run to find out what partitions you can use in your own job script files.)</li>
	<li><strong>QoS</strong>: A QoS is a classification that determines what kind of resources your job can use. For instance, there is a QoS option that you can select for running test jobs when you're debugging your code, which further constrains the resources available to your job and thus may reduce its cost. As well, Condo users can select a "lowprio" QoS which can make use of unused resources on the cluster, in exchange for these jobs being subject to termination when needed, in order to free resources for higher priority jobs. (See below for a command that you can run to find out what QoS options you can use in your own job script files.)</li>
	<li><strong>Time</strong>: A maximum time limit for the job&nbsp;is required under all conditions. When running your job under a QoS that does not have a time limit (such as jobs submitted by the users of some of the cluster's Condos under their priority access QoS), you can specify a sufficiently long time limit value, but this parameter should not be omitted. Jobs submitted without providing a time limit will be rejected by the scheduler.</li>
</ul>

You can view the accounts you have access to, partitions you can use, and the QoS&nbsp;options&nbsp;available to you using the <code>sacct</code><code>mgr</code>&nbsp;command:

<code>sacctmgr -p show associations user=$USER</code>

This will return output such as the following for a hypothetical example user <code>lee</code>&nbsp;who has access to&nbsp;both&nbsp;the <code>physics</code>&nbsp;condo and to a Faculty Computing Allowance. Each line of this output indicates a specific combination of an account, a partition, and QoSes that you can use in a job script file, when submitting any individual batch job:

<code>Cluster|Account|User|Partition|...|QOS|Def QOS|GrpTRESRunMins|
brc|co_physics|lee|savio2_1080ti|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio2_knl|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio2_bigmem|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio2_gpu|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio2_htc|...|savio_lowprio|savio_lowprio||
brc|co_physics|lee|savio_bigmem|...|savio_lowprio|savio_lowprio||</code> <code><strong>brc|co_physics|lee|savio2|...|physics_savio2_normal,savio_lowprio|physics_savio2_normal||</strong></code> <code>brc|co_physics|lee|savio|...|savio_lowprio|savio_lowprio||
brc|fc_lee|lee|savio2_1080ti|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2_knl|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2_bigmem|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2_gpu|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2_htc|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio_bigmem|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio2|...|savio_debug,savio_normal|savio_normal||
brc|fc_lee|lee|savio|...|savio_debug,savio_normal|savio_normal||</code>

The <code>Account</code>, <code>Partition</code>, and&nbsp;<code>QOS</code> fields indicate which partitions and QoSes you have access to under each of your account(s). The <code>Def QoS</code>&nbsp;field identifies the default QoS that will be used if you do not explicitly identify a QoS when submitting a job. Thus as per the example above, if the user <code>lee</code> submitted a batch job using their <code>fc_lee</code> account, they could submit their job to either the <code>savio2_gpu</code>, <code> savio2_htc</code>, <code>savio2_bigmem</code>, <code>savio2</code>, <code>savio</code>, or <code>savio_bigmem</code> partitions. (And when doing so, they could also choose either the <code>savio_debug</code> or <code>savio_normal</code> QoS, with a default of <code>savio_normal</code> if no QoS was specified.)

If you are running your job in a condo, be sure to note which of the line(s) of output associated with the condo account (those beginning with "co_" ) have their <code>Def QoS</code>&nbsp;being a lowprio QoS and which have a normal QoS. Those with a normal QoS (such as the line highlighted in <strong>boldface text</strong> in the above example) are the QoS to which you have priority access, while those with a lowprio QoS are those to which you have only low priority access. Thus, in the above example, the user <code>lee</code> should select the <code>co_physics</code> account and the&nbsp;<code>savio2</code> partition when they want to run jobs with normal priority, using the resources available via their condo membership.

You can find more details on these various partitions and QoS&nbsp;options,&nbsp;as well as on low priority jobs,&nbsp;in the respective "Scheduler" sections of the User Guides for the <a href="http://research-it.berkeley.edu/services/high-performance-computing/user-guide/savio-user-guide#Scheduler" target="_blank">Savio</a>, <a href="http://research-it.berkeley.edu/services/high-performance-computing/cgrl-vectorrosalind-user-guide#Scheduler-Configuration" target="_blank">CGRL (Vector/Rosalind)</a>, and <a href="http://research-it.berkeley.edu/services/high-performance-computing/cortex-user-guide#Scheduler-Configuration" target="_blank">Cortex</a> clusters.

In addition to the key options of account, partition, and QoS, your job script files can also contain <a href="#Job-submission-with-specific-resource-requirements">options to request various numbers of cores, nodes, and/or computational tasks</a>. And there are a variety of <a href="#Additional-job-submission-options">additional options</a> you can specify in your batch files, if desired, such as email notification options when a job has completed. These are all described further below.

<h1 id="Basic-job-submission">Basic job submission</h1>

This section provides an overview of how to run your jobs in <a href="#Batch-jobs">batch (i.e., non-interactive or background) mode</a> and in <a href="#Interactive-jobs">interactive mode</a>.

<h3 id="Batch-jobs">Batch jobs</h3>

When you want to run one of your jobs in batch (i.e. non-interactive or background) mode, you'll enter an <code>sbatch</code>&nbsp;command. As part of that command, you will also specify the name of, or filesystem path to, a SLURM job script file; e.g., <code>sbatch myjob.sh</code>

A job script specifies where and how you want to run your job on the cluster, and ends with the actual command(s) needed to run your job. The job script file looks much like a standard shell script (<code>.sh</code>) file, but also includes one or more lines that specify options for the SLURM scheduler; e.g.

<code>#SBATCH --some-option-here</code>

Although these lines start with hash signs (<code>#</code>), and thus are regarded as comments by the shell, they are nonetheless read and interpreted by the SLURM scheduler.

Here is a minimal example of a job script file that includes the required account, partition, and time options, as well as a qos specification. It will run unattended for up to 30 seconds on one of the compute nodes in the <code>partition_name</code> partition, and will simply print out the words, "hello world":

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
# Quality of Service:
#SBATCH --qos=qos_name
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run:
echo "hello world"
```

In this and other examples, <code>account_name</code>, <code>partition_name</code>, and <code>qos_name</code> are placeholders for actual values you will need to provide. See <a href="#Key-options">Key options to set</a>, above, for information on what values to use in your own job script files.

See <a href="#Job-submission-with-specific-resource-requirements"> Job submission with specific resource requirements</a>, below, for a set of example job script files, each illustrating how to run a specific type of job.

See <a href="#Finding-output">Finding output</a> to learn where output from running your batch jobs can be found. If errors occur when running your batch job, this is the first place to look for these.

<h3 id="Interactive-jobs">Interactive jobs</h3>

In some instances, you may need to use software that requires user interaction, rather than running programs or scripts in batch mode. To do so, you must first start an instance of an interactive shell on a Savio compute node, within which you can then run your software on that node. To run such an interactive job&nbsp;on a compute node, you'll use&nbsp;<code>srun</code>. Here is a&nbsp;basic example&nbsp;that&nbsp;launches an interactive 'bash' shell on that node, and&nbsp;includes the required account and partition options:

<code>[user@ln001 ~]$ srun --pty -A account_name -p partition_name -t 00:00:30 bash -i</code>

Once your job starts, the Linux prompt will change and indicate you are on a compute node rather than a login node:

<code>srun: job 669120 queued and waiting for resources
srun: job 669120 has been allocated resources
[user@n0047 ~]$&nbsp;</code>

<h1 id="Job-submission-with-specific-resource-requirements">Job submission with specific resource requirements</h1>

This section details options for specifying the resource requirements for you jobs. We provide example job scripts for a variety of the kinds of jobs you might submit on Savio.

Remember that nodes are assigned for exclusive access by your job, except in the "savio2_htc" and "savio2_gpu" partitions. So, if possible, you generally want to set SLURM options and write your code to use all the available resources on the nodes assigned to your job (e.g., 20 cores and 64 GB memory per node in the "savio" partition).

<h3 id="Memory">Memory available</h3>

Also note that in all partitions except for GPU and&nbsp;HTC partitions, by default the full memory on the node(s) will be available to your job. On&nbsp;the GPU and HTC partitions you get an amount of memory proportional to the number of CPUs your job requests relative to the number of CPUs on the node. For example, if the node has 64 GB and 8 CPUs, and you request 2 CPUs, you'll have access to 16 GB memory. If you need more memory than that, you should request additional CPUs.

<h3 id="Parallelization">Parallelization</h3>

When submitting parallel code, you usually need to specify the number of tasks, nodes, and CPUs to be used by your job in various ways. For any given use case, there are generally multiple ways to set the options to achieve the same effect; these examples try to illustrate what we consider to be best practices.

The key options for parallelization are:

<ul>
	<li><code>--nodes</code>&nbsp;(or <code>-N</code>): indicates the number of nodes to use</li>
	<li><code>--ntasks-per-node</code>: indicates the number of tasks (i.e., processes one wants to run on each node)</li>
	<li><code>--cpus-per-task</code>&nbsp;(or <code>-c</code>): indicates the number of cpus to be used for each task</li>
</ul>

In addition, in some cases it can make sense to use the <code>--ntasks</code>&nbsp;(or <code>-n</code>) option to indicate the total number of tasks and let the scheduler determine how many nodes and tasks per node are needed. In general --cpus-per-task&nbsp;will be 1 except when running threaded code. &nbsp;

Note that if the various options are not set, SLURM will in some cases infer what the value of the option needs to be given other options that are set and in other cases will treat the value as being 1. So some of the options set in the example below are not strictly necessary, but we give them explicitly to be clear.

<h3 id="Resource-example">Job requesting an entire Savio node</h3>

```
#!/bin/bash
#SBATCH --job-name=test
#SBATCH --account=account_name
#SBATCH --partition=savio
#SBATCH --qos=qos_name
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
#SBATCH --mem=64G&nbsp;&nbsp;
#SBATCH --time=00:00:30
## Command(s) to run:
echo "hello world"
```

Only the partition, time, and account flags are required. And, strictly speaking, the account will default to a default account if you don't specify it, so in some cases that can be omitted.

The following example job scripts are available from <a href="https://github.com/ucberkeley/brc-draft-documentation">GitHub</a>. Each would run a script or program "a.out" in the current directory.

<h3 id="h.ev4l1ht18mr">1. Threaded/OpenMP job script</h3>

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
#QoS:
#SBATCH --qos=qos_name
#
# Request one node:
#SBATCH --nodes=1
#
# Specify one task:
#SBATCH --ntasks-per-node=1
#
# Number of processors for single task needed for use case (example):
#SBATCH --cpus-per-task=4
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run (example):
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
./a.out
```

Here <code>--cpus-per-task</code>&nbsp;should be no more than the number of cores on a Savio node in the partition you request. You may want to experiment with the number of threads for your job to determine the optimal number, as computational speed does not always increase with more threads. Note that if --cpus-per-task is fewer than the number of cores on a node, your job will not make full use of the node. Strictly speaking the <code>--nodes</code>&nbsp;and --ntasks-per-node&nbsp;arguments are optional here because they default to 1.

<h3 id="h.v9dna0kv1ulz">2. Simple multi-core job script (multiple processes on one node)</h3>

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
# QoS:
#SBATCH --qos=qos_name
#
# Request one node:
#SBATCH --nodes=1
#
# Specify number of tasks for use case (example):
#SBATCH --ntasks-per-node=20
#
# Processors per task:
#SBATCH --cpus-per-task=1
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run (example):
./a.out
```

This job script would be appropriate for multi-core R, Python, or MATLAB jobs. In the commands that launch your code and/or within your code itself, you can reference the <code>SLURM_NTASKS</code>&nbsp;environment variable to dynamically identify how many tasks (i.e., processing units) are available to you.

Here the number of CPUs used by your code at at any given time should be no more than the number of cores on a Savio node.

For a way to run many individual jobs on one or more nodes (more jobs than cores), see the Tips &amp; Tricks entry regarding use of a <a href="http://research-it.berkeley.edu/services/high-performance-computing/tips-using-brc-savio-cluster#q-how-to-run-high-throughput-computing-htc-type-of-jobs-how-to-run-multiple-jobs-on-the-same-node-"> mini-scheduler script</a>.

<h3 id="h.ql3ucviv3ve6">3. MPI job script</h3>

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
# QoS:
#SBATCH --qos=qos_name
#
# Number of MPI tasks needed for use case (example):
#SBATCH --ntasks=40
#
# Processors per task:
#SBATCH --cpus-per-task=1
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run (example):
module load gcc openmpi
mpirun ./a.out
```

As noted in the introduction, for all partitions except for <code>savio2_htc</code> and <code>savio2_gpu</code>, you probably want to set the number of tasks to be a multiple of the number of cores per node in that partition, thereby making use of all the cores on the node(s) to which your job is assigned.

This example assumes that each task will use a single core; otherwise there could be resource contention amongst the tasks assigned to a node.

<h3 id="h.1lsdgn8pa5j7">4. Alternative MPI job script</h3>

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
# QoS:
#SBATCH --qos=qos_name
#
# Number of nodes needed for use case:
#SBATCH --nodes=2
#
# Tasks per node based on number of cores per node (example):
#SBATCH --ntasks-per-node=20
#
# Processors per task:
#SBATCH --cpus-per-task=1
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run (example):
module load gcc openmpi
mpirun ./a.out
```

This alternative explicitly specifies the number of nodes, tasks per node, and CPUs per task rather than simply specifying the number of tasks and having SLURM determine the resources needed. As before, one would generally want the number of tasks per node to equal a multiple of the number of cores on a node, assuming only one CPU per task.

<h3 id="h.1mt6bqkoxbwr">5. Hybrid OpenMP+MPI job script</h3>

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
# QoS:
#SBATCH --qos=qos_name
#
# Number of nodes needed for use case (example):
#SBATCH --nodes=2
#
# Tasks per node based on --cpus-per-task below and number of cores
# per node (example):
#SBATCH --ntasks-per-node=4
#
# Processors per task needed for use case (example):
#SBATCH --cpus-per-task=5
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run (example):
module load gcc openmpi
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
mpirun ./a.out
```

Here we request a total of 8 (=2x4) MPI tasks, with 5 cores per task. &nbsp;This would make use of all the cores on two, 20-core nodes in the "savio" partition.

<h3 id="h.ic6ho8nvht16">6. Jobs scheduled on a per-core basis (jobs that use fewer cores than available on a node)</h3>

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# QoS:
#SBATCH --qos=qos_name
#
# Partition:
#SBATCH --partition=savio2_htc
#
# Number of tasks needed for use case (example):
#SBATCH --ntasks=4
#
# Processors per task:
#SBATCH --cpus-per-task=1
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run (example):
./a.out
```

In the <code>savio2_htc</code> pool you are only charged for the actual number of cores used, so the notion of making best use of resources by saturating a node is not relevant.

<h3 id="h.xaueegcmzphs">7. GPU job script</h3>

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=savio2_gpu
#
# QoS:
#SBATCH --qos=qos_name
#
# Number of nodes:
#SBATCH --nodes=1
#
# Number of tasks (one for each GPU desired for use case) (example):
#SBATCH --ntasks=1
#
# Processors per task (please always specify the total number of processors twice the number of GPUs):
#SBATCH --cpus-per-task=2
#
#Number of GPUs, this can be in the format of "gpu:[1-4]", or "gpu:K80:[1-4] with the type included
#SBATCH --gres=gpu:1
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run (example):
./a.out
```

To help the job scheduler effectively manage the use of GPUs, <em>your job submission script must request two CPUs for each GPU you will use</em>. Jobs submitted that do not request a minimum of two CPUs for every GPU will be rejected by the scheduler.

Here’s how to request two CPUs for each GPU: the total of CPUs requested results from multiplying two settings: the <em>number of tasks</em> (“--ntasks=”) and <em>CPUs per task</em> ("--cpus-per-task=").

For instance, in the above example, one GPU was requested via “--gres=gpu:1”, and the required total of two CPUs was thus requested via the combination of “--ntasks=1” and "--cpus-per-task=2" . Similarly, if your job script requests four GPUs via "--gres=gpu:4", and uses "--ntasks=8", it should also include "--cpus-per-task=1" in order to request the required total of eight CPUs.

Note that the "--gres=gpu:[1-4]" specification must be between 1 and 4. This is because the feature is associated with a node, and the nodes each have 4 GPUs. If you wish to use more than 4 GPUs, your&nbsp;"--gres=gpu:[1-4]" specification should include how many GPUs to use per node requested. For example, if you wish to use eight&nbsp;GPUs, your job script should include options to the effect of&nbsp;"--gres=gpu:4", "--nodes=2", "--ntasks=8", and "--cpus-per-task=2".

<h3 id="h.ic6ho8nvht16">8. Long-running jobs (up to 10 days and 4 cores per job)</h3>

```
#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# QoS: must be savio_long for jobs &gt; 3 days
#SBATCH --qos=savio_long
#
# Partition:
#SBATCH --partition=savio2_htc
#
# Number of tasks needed for use case (example):
#SBATCH --ntasks=2
#
# Processors per task:
#SBATCH --cpus-per-task=1
#
# Wall clock limit (7 days in this case):
#SBATCH --time=7-00:00:00
#
## Command(s) to run (example):
./a.out
```

A given job in the long queue can use no more than 4 cores and a maximum of 10 days. Collectively across the entire Savio cluster, at most 24 cores are available for long-running jobs, so you may find that your job may sit in the queue for a while before it starts.

In the <code>savio2_htc</code> pool you are only charged for the actual number of cores used, so the notion of making best use of resources by saturating a node is not relevant.

<h3 id="h.ic6ho8nvht16">9. Low-priority jobs</h3>

Your job will use a default Qos (generally <code>savio_normal</code>) if not specified. To use the low-priority queue, you need to specify the low-priority QoS, as follows.

<code>#!/bin/bash
# Job name:
#SBATCH --job-name=test
#
# Account:
#SBATCH --account=account_name
#
# Partition:
#SBATCH --partition=partition_name
#
# Quality of Service:
#SBATCH --qos=savio_lowprio
#
# Wall clock limit:
#SBATCH --time=00:00:30
#
## Command(s) to run:
echo "hello world"</code>

<a id="saviolong" name="saviolong"></a>

<h1 id="Additional-job-submission-options">Additional job submission options</h1>

Here are some additional options that you can incorporate as needed into your own scripts. For the full set of available options, please see the <a href="http://slurm.schedmd.com/sbatch.html"> SLURM documentation on the sbatch command</a>.

<h3 id="Finding-output">Output options</h3>

Output from running a SLURM batch job is, by default, placed in a log file named <code>slurm-%j.out</code>, where the job's ID is substituted for <code>%j</code>; e.g. <code>slurm-478012.out</code> This file will be created in your current directory; i.e. the directory from within which you entered the sbatch command. Also by default, both command output and error output (to stdout and stderr, respectively) are combined in this file.

To specify alternate files for command and error output use:

<ul>
	<li><code>--output</code>: destination file for stdout</li>
	<li><code>--error</code>: destination file for stderr</li>
</ul>

<h3 id="Email-options">Email Notifications</h3>

By specifying your email address, the SLURM scheduler can email you when the status of your job changes. Valid options are BEGIN, END, FAIL, REQUEUE, and ALL, and multiple options can be separated by commas.

The required options for email notifications are:

<ul>
	<li><code>--mail-type</code>: when you want to be notified</li>
	<li><code>--mail-user</code>: your email address</li>
</ul>

<h3 id="Email-example">Submission with output and email options</h3>

<code>#!/bin/bash
#SBATCH --job-name=test
#SBATCH --account=account_name
#SBATCH --partition=savio
#SBATCH --qos=qos_name
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
#SBATCH --mem=64G
#SBATCH --time=00:00:30
<strong>#SBATCH --output=test_job_%j.out</strong>
<strong>#SBATCH --error=test_job_%j.err</strong>
<strong>#SBATCH --mail-type=END,FAIL</strong>
<strong>#SBATCH --mail-user=jessie.doe@berkeley.edu</strong>
## Command(s) to run:
echo "hello world"</code>

<h3 id="Array-jobs">QoS options</h3>

While your job will use a default QoS, generally <code>savio_normal</code>, you can specify a different QoS, such as the debug QoS for short jobs, using the <code>--qos</code> flag, e.g.,

<code>#!/bin/bash
#SBATCH --job-name=test
#SBATCH --account=account_name
#SBATCH --partition=savio
<strong>#SBATCH --qos=savio_debug</strong>
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
##SBATCH --time=00:00:30
## Command(s) to run:
echo "hello world"</code>

<h3 id="Array-jobs">Job Script with Job Array</h3>

Job arrays allow many jobs to be submitted simultaneously with the same requirements. Within each job the environment variable <code>$SLURM_ARRAY_TASK_ID</code> is set and can be used to alter the execution of each job in the array.

By default, output from job arrays is placed in a series of log files named <code>slurm-%A_%a.out</code>, where <code>%A</code> is the overall job ID and <code>%a</code> is the task ID.

For example, the following script would write "I am task 0" to <code>array_job_XXXXXX_task_0.out</code>, "I am task 1" to <code>array_job_XXXXXX_task_1.out</code>, etc, where XXXXXX is the job ID.

<code>#!/bin/bash
#SBATCH --job-name=test
#SBATCH --account=account_name
#SBATCH --partition=savio
#SBATCH --qos=qos_name
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
#SBATCH --mem=64G
#SBATCH --time=00:00:30
<strong>#SBATCH --array=0-31</strong>
<strong>#SBATCH --output=array_job_%A_task_%a.out</strong>
<strong>#SBATCH --error=array_job_%A_task_%a.err</strong>
## Command(s) to run:
echo "I am task $SLURM_ARRAY_TASK_ID"</code>

<h1 id="Monitoring">Monitoring the status of running batch jobs</h1>

To monitor a running job, you need to know the SLURM job ID of that job, which can be obtained by running

<code>squeue -u $USER</code>

In the commands below, substitute the job ID for "$your_job_id".

If you suspect your job is not running properly, or you simply want to understand how much memory or how much CPU the job is actually using on
the compute nodes, Savio provides a script "wwall" to check that.

The following provides a snapshot of node status that the job is running on:

<code>wwall -j $your_job_id</code>

while

<code>wwall -j $your_job_id -t </code>

provides a text-based user interface (TUI) to monitor the node status when the job progresses. To exit the TUI, enter "q" to quit out of the
interface and be returned to the command line.

Alternatively, you can login to the node your job is running on as follows:

<code>srun --jobid=$your_job_id --pty /bin/bash</code>

This runs a shell in the context of your existing job. Once on the node, you can run <code>top</code>, <code>ps</code>, or other tools.

<h1 id="Migration">Migrating from Torque/PBS to SLURM</h1>

The purpose of this section is to help users who are familiar with Torque/PBS to migrate to the SLURM environment more efficiently. For users coming from other schedulers, such as Platform LSF, SGE/OGE, Load Leveler, please use <a href="http://slurm.schedmd.com/rosetta.pdf"> this link</a>&nbsp;to find a quick reference.

Table 1 lists the common tasks that you can perform&nbsp;in Torque/PBS and the equivalent ways to perform those tasks&nbsp;in SLURM.

<table border="1">
<tbody>
<tr>
<td colspan="1" rowspan="1">Task</td>
<td colspan="1" rowspan="1">Torque/PBS</td>
<td colspan="1" rowspan="1">SLURM</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Submit a job</td>
<td colspan="1" rowspan="1">qsub myjob.sh</td>
<td colspan="1" rowspan="1">sbatch myjob.sh</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Delete a job</td>
<td colspan="1" rowspan="1">qdel 123</td>
<td colspan="1" rowspan="1">scancel 123</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show job status</td>
<td colspan="1" rowspan="1">qstat</td>
<td colspan="1" rowspan="1">squeue</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show expected job start time</td>
<td colspan="1" rowspan="1">- (showstart in Maui/Moab)</td>
<td colspan="1" rowspan="1">squeue --start</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show queue info</td>
<td colspan="1" rowspan="1">qstat -q</td>
<td colspan="1" rowspan="1">sinfo</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show job details</td>
<td colspan="1" rowspan="1">qstat -f 123</td>
<td colspan="1" rowspan="1">scontrol show job 123</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show queue details</td>
<td colspan="1" rowspan="1">qstat -Q -f &lt;queue&gt;</td>
<td colspan="1" rowspan="1">scontrol show partition &lt;partition_name&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show node details</td>
<td colspan="1" rowspan="1">pbsnode n0000</td>
<td colspan="1" rowspan="1">scontrol show node n0000</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Show QoS details</td>
<td colspan="1" rowspan="1">- (mdiag -q &lt;QoS&gt; in Maui/Moab)</td>
<td colspan="1" rowspan="1">sacctmgr show qos &lt;QoS&gt;</td>
</tr>
</tbody></table>

Table 2 lists the commonly used options in the batch job script for both Torque/PBS (qsub) and SLURM (sbatch/srun/salloc).

<table border="1">
<tbody>
<tr>
<td colspan="1" rowspan="1">Option</td>
<td colspan="1" rowspan="1">Torque/PBS</td>
<td colspan="1" rowspan="1">SLURM</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares the time after which the job is eligible for execution.</td>
<td colspan="1" rowspan="1">-a date_time</td>
<td colspan="1" rowspan="1">--begin=&lt;time&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the account string associated with the job.</td>
<td colspan="1" rowspan="1">-A account_string</td>
<td colspan="1" rowspan="1">-A, --account=&lt;account&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the path to be used for the standard error stream of the batch job.</td>
<td colspan="1" rowspan="1">-e [hostname:][path_name]</td>
<td colspan="1" rowspan="1">-e, --error=&lt;filename pattern&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies that a user hold be applied to the job at submission time.</td>
<td colspan="1" rowspan="1">-h</td>
<td colspan="1" rowspan="1">-H, --hold</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares that the job is to be run "interactively".</td>
<td colspan="1" rowspan="1">-I</td>
<td colspan="1" rowspan="1">srun -u bash -i</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares if the standard error stream of the job will be merged with the standard output stream of the job.</td>
<td colspan="1" rowspan="1">-j oe / -j eo</td>
<td colspan="1" rowspan="1">default behavior</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Requests a number of nodes be allocated to this job.</td>
<td colspan="1" rowspan="1">-l nodes=number</td>
<td colspan="1" rowspan="1">-n, --ntasks=&lt;number&gt; / -N, --nodes=&lt;minnodes[-maxnodes]&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the number of processors per node requested.</td>
<td colspan="1" rowspan="1">-l nodes=number:ppn=number</td>
<td colspan="1" rowspan="1">--ntasks-per-node=&lt;ntasks&gt; / --tasks-per-node=&lt;n&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the node feature.</td>
<td colspan="1" rowspan="1">-l nodes=number:gpu</td>
<td colspan="1" rowspan="1">-C, --constraint="gpu"</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Requests a specific list of node names.</td>
<td colspan="1" rowspan="1">-l nodes=node1+node2</td>
<td colspan="1" rowspan="1">-w, --nodelist=&lt;node name list&gt; / -F, --nodefile=&lt;node file&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the real memory required per node in Megabytes.</td>
<td colspan="1" rowspan="1">-l mem=mem</td>
<td colspan="1" rowspan="1">--mem=&lt;MB&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the minimum&nbsp;memory required per allocated CPU in Megabytes.</td>
<td colspan="1" rowspan="1">no equivalent</td>
<td colspan="1" rowspan="1">--mem-per-cpu=&lt;MB&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Requests a quality of service for the job.</td>
<td colspan="1" rowspan="1">-l qos=qos</td>
<td colspan="1" rowspan="1">--qos=&lt;qos&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Sets a limit on the total run time of the job allocation.</td>
<td colspan="1" rowspan="1">-l walltime=time</td>
<td colspan="1" rowspan="1">-t, --time=&lt;time&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the set of conditions under which the execution server will send a mail message about the job.</td>
<td colspan="1" rowspan="1">-m mail_options (a, b, e)</td>
<td colspan="1" rowspan="1">--mail-type=&lt;type&gt; (type = BEGIN, &nbsp;END, &nbsp;FAIL, REQUEUE, ALL)</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares the list of users to whom mail is sent by the execution server when it sends mail about the job.</td>
<td colspan="1" rowspan="1">-M user_list</td>
<td colspan="1" rowspan="1">--mail-user=&lt;user&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies that the job has exclusive access to the nodes it is executing on.</td>
<td colspan="1" rowspan="1">-n</td>
<td colspan="1" rowspan="1">--exclusive</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares a name for the job.</td>
<td colspan="1" rowspan="1">-N name</td>
<td colspan="1" rowspan="1">-J, --job-name=&lt;jobname&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the path to be used for the standard output stream of the batch job.</td>
<td colspan="1" rowspan="1">-o path</td>
<td colspan="1" rowspan="1">-o, --output=&lt;filename pattern&gt;</td>
</tr>
</tbody></table>

<table border="1">
<tbody>
<tr>
<td colspan="1" rowspan="1">Defines the destination of the job.</td>
<td colspan="1" rowspan="1">-q destination</td>
<td colspan="1" rowspan="1">-p, --partition=&lt;partition_names&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares whether the job is rerunnable.</td>
<td colspan="1" rowspan="1">-r y|n</td>
<td colspan="1" rowspan="1">--requeue</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares the shell that interprets the job script.</td>
<td colspan="1" rowspan="1">-S path_list</td>
<td colspan="1" rowspan="1">no equivalent</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Specifies the task ids of a job array.</td>
<td colspan="1" rowspan="1">-t array_request</td>
<td colspan="1" rowspan="1">-a, --array=&lt;indexes&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Allows for per job prologue and epilogue scripts.</td>
<td colspan="1" rowspan="1">-T script_name</td>
<td colspan="1" rowspan="1">no equivalent</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the user name under which the job is to run on the execution system.</td>
<td colspan="1" rowspan="1">-u user_list</td>
<td colspan="1" rowspan="1">--uid=&lt;user&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Expands the list of environment variables that are exported to the job.</td>
<td colspan="1" rowspan="1">-v variable_list</td>
<td colspan="1" rowspan="1">--export=&lt;environment variables | ALL | NONE&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Declares that all &nbsp;environment variables in the qsub command's environment are to be exported to the batch job.</td>
<td colspan="1" rowspan="1">-V</td>
<td colspan="1" rowspan="1">--export=&lt;environment variables | ALL | NONE&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the working directory path to be used for the job.</td>
<td colspan="1" rowspan="1">-w path</td>
<td colspan="1" rowspan="1">-D, --workdir=&lt;directory&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job may be scheduled for execution at any point after jobs jobid have started &nbsp;execution.</td>
<td colspan="1" rowspan="1">-W depend=after:jobid[:jobid...]</td>
<td colspan="1" rowspan="1">-d, --dependency=after:job_id[:jobid...]</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job may be scheduled &nbsp;for execution only after jobs jobid have terminated with no errors.</td>
<td colspan="1" rowspan="1">-W depend=afterok:jobid[:jobid...]</td>
<td colspan="1" rowspan="1">-d, --dependency=afterok:job_id[:jobid...]</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job may be scheduled for execution only after jobs jobid have terminated with errors.</td>
<td colspan="1" rowspan="1">-W depend=afternotok:jobid[:jobid...]</td>
<td colspan="1" rowspan="1">-d, --dependency=afternotok:job_id[:jobid...]</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job may be scheduled for execution after jobs jobid have terminated, with or without errors.</td>
<td colspan="1" rowspan="1">-W depend=afterany:jobid[:jobid...]</td>
<td colspan="1" rowspan="1">-d, --dependency=afterany:job_id[:jobid...]</td>
</tr>
<tr>
<td colspan="1" rowspan="1">This job can begin execution after any previously launched jobs sharing the same job name and user have terminated.</td>
<td colspan="1" rowspan="1">no equivalent</td>
<td colspan="1" rowspan="1">-d, --dependency=singleton</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Defines the group name under which the job is to run on the execution system.</td>
<td colspan="1" rowspan="1">-W group_list=g_list</td>
<td colspan="1" rowspan="1">--gid=&lt;group&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Allocates resources for the job from the named reservation.</td>
<td colspan="1" rowspan="1">-W x=FLAGS:ADVRES:staff.1</td>
<td colspan="1" rowspan="1">--reservation=&lt;name&gt;</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Enables X11 forwarding.</td>
<td colspan="1" rowspan="1">-X</td>
<td colspan="1" rowspan="1">srun --pty [command]</td>
</tr>
</tbody></table>

Table 3 lists the commonly used environment variables in Torque/PBS and the equivalents in SLURM.

<table border="1">
<tbody>
<tr>
<td colspan="1" rowspan="1">Environment Variable&nbsp;For</td>
<td colspan="1" rowspan="1">Torque/PBS</td>
<td colspan="1" rowspan="1">SLURM</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Job ID</td>
<td colspan="1" rowspan="1">PBS_JOBID</td>
<td colspan="1" rowspan="1">SLURM_JOB_ID / SLURM_JOBID</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Job name</td>
<td colspan="1" rowspan="1">PBS_JOBNAME</td>
<td colspan="1" rowspan="1">SLURM_JOB_NAME</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Node list</td>
<td colspan="1" rowspan="1">PBS_NODELIST</td>
<td colspan="1" rowspan="1">SLURM_JOB_NODELIST / SLURM_NODELIST</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Job submit directory</td>
<td colspan="1" rowspan="1">PBS_O_WORKDIR</td>
<td colspan="1" rowspan="1">SLURM_SUBMIT_DIR</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Job array ID (index)</td>
<td colspan="1" rowspan="1">PBS_ARRAY_INDEX</td>
<td colspan="1" rowspan="1">SLURM_ARRAY_TASK_ID</td>
</tr>
<tr>
<td colspan="1" rowspan="1">Number of tasks</td>
<td colspan="1" rowspan="1">-</td>
<td colspan="1" rowspan="1">SLURM_NTASKS</td>
</tr>
</tbody></table>
