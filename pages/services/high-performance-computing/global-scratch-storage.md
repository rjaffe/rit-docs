---
title: Global Scratch Storage
keywords: high performance computing, berkeley research computing, status, announcements
last_updated: October 15, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/global-scratch-storage
folder: hpc
---

<p>The purpose of this document is to provide guidelines for using the global scratch storage space connected to the Berkeley Research Computing supercluster.</p>
<p>Every Savio user has a scratch space located at <code>/global/scratch/&lt;username&gt;</code>. There are no limits on the use of the scratch space, though it is an 1.5 PB resource shared among all users.</p>
<p>Files stored in this space are not backed up; the recommended use cases for scratch storage are:</p>
<ul><li>Working space for your running jobs (temporary files, checkpoint data, etc.)</li>
<li>High performance input and output</li>
<li>Large input/output files</li>
</ul><p><strong>Please remember that global scratch storage is a shared resource.</strong> We strongly urge users to regularly clean up their data in global scratch to decrease scratch storage usage. Users with many terabytes of data may be requested to reduce their usage, if global scratch becomes full.</p>
<p>BRC has an ongoing inactive data purge policy for the scratch area. Any file that has not been accessed in at least 6 months will be subject to deletion. If you need to retain access to data on the cluster for more than 6 months between uses, you can consider purchasing storage space through the <a href="http://research-it.berkeley.edu/services/high-performance-computing/brc-condo-storage-service-savio">condo storage program</a>.</p>
<p>If you have any questions or concerns about global scratch storage, please contact us at <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>. If you have other concerns about managing your research data, please contact Research Data Management at <a href="mailto:resear
