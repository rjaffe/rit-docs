---
title: Accessing and Installing Software
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/accessing-and-installing-software

---

<h3><a name="Overview" id="Overview">1. Overview</a></h3>
<p>To access much of the software available on the Savio cluster - ranging from compilers and interpreters to statistical analysis and visualization software, and much more - you'll use <a href="#Examples" class="toc-filter-processed">Environment Module commands</a>.</p>
<p>These commands allow you to display a list of many of the <a href="#Software_Provided" class="toc-filter-processed">software packages provided on the cluster</a>, as well as to conveniently load and unload different packages and their associated runtime environments, as you need them.</p>
<p>As a quick overview, Environment Modules are used to manage users’ runtime environments dynamically on the Savio cluster. This is accomplished by loading and unloading modulefiles which contain the application specific information for setting a user’s environment, primarily the shell environment variables, such as <em>PATH</em>, <em>LD_LIBRARY_PATH</em>, etc. Modules are useful in managing different applications, as well as different versions of the same application, in a cluster environment.</p>
<p>Finally, in addition to the software provided on the Savio cluster, you're also welcome to <a href="#Installing" class="toc-filter-processed">install your own software</a>.</p>
<h3><a name="Examples" id="Examples">2. Environment Modules Usage Examples</a></h3>
<p>Below are some of the Environment Module commands that you'll be using most frequently. All of these commands begin with <kbd>module</kbd> and are followed by a subcommand. (In this list of commands, a vertical bar (“|”) means “or”, e.g., <kbd>module add</kbd> and <kbd>module load</kbd> are equivalent. And you'll need to substitute actual modulefile names for <em><kbd>modulefile</kbd></em>, <em><kbd>modulefile1</kbd></em>, and <em><kbd>modulefile2 </kbd></em>in the examples below.)</p>
<ul class="rteindent1"><li><kbd>module avail</kbd> - List all available modulefiles in the current MODULEPATH. (This is the command to use when you want to see the list of software that you can use on Savio via Environment Module commands.)</li>
<li><kbd>module list</kbd> - List loaded modules. (This shows you what software you currently have available in your environment. <strong>Please note that, by default, no modules are loaded.</strong>)</li>
<li><kbd>module add|load <em>modulefile</em> ...</kbd> - Load modulefile(s) into the shell environment. (This allows you to add more software packages to your environment.)</li>
<li><kbd>module rm|unload <em>modulefile</em> ...</kbd> - Remove modulefile(s) from the shell environment.</li>
<li><kbd>module swap|switch [<em>modulefile1</em>] <em>modulefile2</em> - </kbd>Switch loaded<kbd> <em>modulefile1</em> </kbd>with<kbd> <em>modulefile2</em>.</kbd></li>
<li><kbd>module show|display <em>modulefile</em> ...</kbd> - Display configuration information about the specified modulefile(s).</li>
<li><kbd>module whatis [<em>modulefile</em> ...]</kbd> - Display summary information about the specified modulefile(s).</li>
<li><kbd>module purge</kbd> - Unload all loaded modulefiles.</li>
</ul><p>For more detailed usage instructions for the <strong>module</strong> command, please run <kbd>man module</kbd> on the cluster.</p>
<p>Below are representative examples of how to use these commands. Depending on which system you have access to and when you are reading this instruction, what you see here could be different from the actual output from the system that you work on. On systems like Savio, where a hierarchical structure is used, some modulefiles will only be available after their root modulefile is loaded. (For instance, libraries for C compilers, and the like, will only become available after their respective parent modules have been loaded.)</p>
<p>It can be helpful to try out each of the following examples in sequence, to more fully understand how environment modules work. Commands you'll enter are shown in <strong>bold</strong>, followed by samples of output you might see:</p>
<div class="rteindent1">
<p><code>[casey@n0000 ~]$ <strong>module avail</strong></code></p>
<p><code>---- /global/software/sl-7.x86_64/modfiles/langs ----<br>
clang/3.9.1&nbsp; cuda/8.0&nbsp; gcc/4.8.5&nbsp; gcc/6.3.0&nbsp; java/1.8.0_121&nbsp; python/2.7&nbsp; python/3.6&nbsp; r/3.4.2</code></p>
<p><code>---- /global/software/sl-7.x86_64/modfiles/tools ----<br>
arpack-ng/3.4.0&nbsp; gflags/2.2.0&nbsp; gv/3.7.4&nbsp; lmdb/0.9.19&nbsp; nano/2.7.4&nbsp;&nbsp; qt/5.4.2&nbsp; texlive/2016</code></p>
<p><code>---- /global/software/sl-7.x86_64/modfiles/apps ----<br>
bio/blast/2.6.0&nbsp; math/octave/current&nbsp; ml/mxnet/0.9.3-py35&nbsp; ml/theano/current-py36&nbsp; </code></p>
<p><code>.</code><code>..</code></p>
<p><code>[casey@n0000 ~]$ <strong>module list</strong><br>
No Modulefiles Currently Loaded.</code></p>
<p><code>[casey@n0000 ~]$ <strong>module load intel</strong></code></p>
<p><code>[casey@n0000 ~]$ <strong>module list</strong></code></p>
<p><code>Currently Loaded Modulefiles:<br>
&nbsp; 1) intel/2016.4.072</code></p>
<p><code>[casey@n0000 ~]$ <strong>module load openmpi mkl</strong></code></p>
<p><code>[casey@n0000 ~]$ <strong>module list</strong><br>
Currently Loaded Modulefiles:<br>
&nbsp; 1) intel/2016.4.072 &nbsp; 3) mkl/2016.4.072<br>
&nbsp; 2) openmpi/2.0.2-intel</code></p>
<p><code>[casey@n0000 ~]$ <strong>module unload openmpi</strong><br>
[casey@n0000 ~]$ <strong>module list</strong><br>
Currently Loaded Modulefiles:<br>
&nbsp; 1) intel/2016.4.072 &nbsp; 2) mkl/2016.4.072</code></p>
<p><code>[casey@n0000 ~]$ <strong>module switch mkl lapack</strong><br>
[casey@n0000 ~]$ <strong>module list</strong><br>
Currently Loaded Modulefiles:<br>
&nbsp; 1) intel/2016.4.072&nbsp;&nbsp; 2) lapack/3.8.0-intel</code></p>
<p><code>[casey@n0000 ~]$ <strong>module show mkl</strong></code></p>
<p><code>-------------------------------------------------------------------<br>
/global/software/sl-7.x86_64/modfiles/intel/2016.4.072/mkl/2016.4.072:</code></p>
<p><code>module-whatis&nbsp;&nbsp;&nbsp; This module sets up MKL 2016.4.072 in your environment.<br>
setenv&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; MKL_DIR /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl<br>
setenv&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; MKLROOT /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl<br>
prepend-path&nbsp;&nbsp; &nbsp; CPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include<br>
prepend-path&nbsp;&nbsp; &nbsp; CPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include/fftw<br>
prepend-path&nbsp;&nbsp; &nbsp; FPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include<br>
prepend-path&nbsp;&nbsp; &nbsp; FPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include/fftw<br>
prepend-path&nbsp;&nbsp; &nbsp; INCLUDE /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include<br>
prepend-path&nbsp;&nbsp; &nbsp; INCLUDE /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/include/fftw<br>
prepend-path&nbsp;&nbsp; &nbsp; LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin<br>
prepend-path&nbsp;&nbsp; &nbsp; LD_LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin<br>
prepend-path&nbsp;&nbsp; &nbsp; MIC_LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin_mic<br>
prepend-path&nbsp;&nbsp; &nbsp; MIC_LD_LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin_mic<br>
prepend-path&nbsp;&nbsp; &nbsp; SINK_LD_LIBRARY_PATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin_mic<br>
prepend-path&nbsp;&nbsp; &nbsp; NLSPATH /global/software/sl-7.x86_64/modules/langs/intel/2016.4.072/mkl/lib/intel64_lin/locale/en_US<br>
-------------------------------------------------------------------</code></p>
<p><code>[casey@n0000 ~]$ <strong>module whatis mkl</strong><br>
mkl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : This module sets up MKL 2016.4.072 in your environment.</code></p>
<p><code>[casey@n0000 ~]$ <strong>module purge</strong> &nbsp;<br>
[casey@n0000 ~]$ <strong>module list</strong><br>
No Modulefiles Currently Loaded.</code></p>
<p><code>[casey@n0000 ~]$ <strong>module avail</strong></code></p>
<p><code>---- /global/software/sl-7.x86_64/modfiles/langs ----<br>
clang/3.9.1&nbsp; cuda/8.0&nbsp; gcc/4.8.5&nbsp; gcc/6.3.0&nbsp; java/1.8.0_121&nbsp; python/2.7&nbsp; python/3.6&nbsp; r/3.4.2</code></p>
<p><code>---- /global/software/sl-7.x86_64/modfiles/tools ----<br>
arpack-ng/3.4.0&nbsp; gflags/2.2.0&nbsp; gv/3.7.4&nbsp; lmdb/0.9.19&nbsp; nano/2.7.4&nbsp;&nbsp; qt/5.4.2&nbsp; texlive/2016</code></p>
<p><code>---- /global/software/sl-7.x86_64/modfiles/apps ----<br>
bio/blast/2.6.0&nbsp; math/octave/current&nbsp; ml/mxnet/0.9.3-py35&nbsp; ml/theano/current-py36&nbsp; </code></p>
<p><code>.</code><code>..</code></p>
<p><code>[casey@n0000 ~]$ <strong>module load gcc</strong></code></p>
<p><code>[casey@n0000 ~]$ <strong>module avail</strong></code></p>
<p><code>---- /global/software/sl-7.x86_64/modfiles/langs ----<br>
clang/3.9.1&nbsp; cuda/8.0&nbsp; gcc/4.8.5&nbsp; gcc/6.3.0&nbsp; java/1.8.0_121&nbsp; python/2.7&nbsp; python/3.6&nbsp; r/3.4.2</code></p>
<p><code>---- /global/software/sl-7.x86_64/modfiles/tools ----<br>
arpack-ng/3.4.0&nbsp; gflags/2.2.0&nbsp; gv/3.7.4&nbsp; lmdb/0.9.19&nbsp; nano/2.7.4&nbsp;&nbsp; qt/5.4.2&nbsp; texlive/2016</code></p>
<p><code>---- /global/software/sl-7.x86_64/modfiles/gcc/6.3.0 ----<br>
antlr/2.7.7-gcc&nbsp; fftw/2.1.5-gcc&nbsp; hdf5/1.8.18-gcc-p&nbsp; ncl/6.3.0-gcc&nbsp; ncview/2.1.7-gcc&nbsp; openmpi/2.0.2-gcc</code></p>
<p>...<br><strong>NOTE: Some modulefiles will become available only after the “<em>intel</em>” or&nbsp;“<em>gcc</em>” modulefile is loaded, for example openmpi, hdf5, and netcdf.&nbsp;</strong>To view an extensive list of all modules including those only visible after loading other prerequisite modules, enter the following command:</p>
<p><code>find /global/software/sl-7.x86_64/modfiles -type d -exec ls -d {} \;</code></p>
</div>
<h3><a id="Software_Provided" name="Software_Provided">3. Software Provided on Savio</a></h3>
<p>Research&nbsp;IT provides and maintains a set of system level software modules. The purpose is to provide an ecosystem that most users can rely on to accomplish their research and studies. The range of applications and libraries that Research&nbsp;IT supports highly depend on the use case and the frequency of how often a support request is received.</p>
<p>For a detailed and up-to-date list of software provided on the cluster, run the <kbd>module avail</kbd> command, as described in <a href="#Examples" class="toc-filter-processed">the usage examples above</a>.</p>
<p>(Note: if you're interested in whether a particular C library is provided on the cluster, make sure that you first load the parent software itself – namely the Intel or GCC compilers – before checking the list of provided software, as that list is dynamically adjusted based on your current environment.)</p>
<p>Currently the following categories of applications and libraries are supported, with some key examples of each shown below:</p>
<ul class="rteindent1"><li>Development Tools</li>
<li>Data processing, Machine Learning, and Visualization Tools</li>
<li>Typesetting and Publishing Tools</li>
<li>Miscellaneous Tools (examples not yet listed below)<br>
&nbsp;</li>
</ul><table align="center" border="1" cellpadding="0" cellspacing="0"><thead><tr><th style="text-align:center">Category</th>
<th style="text-align:center">Application/Library Name</th>
</tr></thead><tbody><tr style="background-color:#D3D3D3"><td align="center" colspan="2">Development Tools</td>
</tr><tr><th scope="row">Editor/IDE</th>
<td>Emacs, Vim, Nano, cmake, cscope, ctags</td>
</tr><tr><th scope="row">SCM</th>
<td>Git, Mercurial</td>
</tr><tr><th scope="row">Debugger/Profiler/Tracer</th>
<td>GDB, gprof, Valgrind, TAU, Allinea DDT</td>
</tr><tr><th scope="row">Languages/Platforms</th>
<td>GCC, Intel, Perl, Python, Java, Boost, CUDA, UPC, Open MPI, TBB, MPI4Py, Python / IPython, R, MATLAB (license required), Octave, Julia</td>
</tr><tr><th scope="row">Math Libraries</th>
<td>MKL, ATLAS, FFTW, FFTW3, GSL, LAPACK, ScaLAPACK, NumPy, SciPy, Eigen</td>
</tr><tr><th scope="row">IO Libraries</th>
<td>HDF5, NetCDF, NCO, NCL</td>
</tr><tr style="background-color:#D3D3D3"><td align="center" colspan="2">Data processing, Visualization, and Machine Learning Tools</td>
</tr><tr><th scope="row">Data Processing/Visualization</th>
<td>Gnuplot, Grace, Graphviz, ImageMagick, MATLAB (license required), Octave, ParaView, Python / IPython, R, VisIt, VMD, yt, Matplotlib</td>
</tr><tr><th scope="row">Machine Learning</th>
<td>Tensorflow, Caffe, H2O, MXNet, Theano, Torch, Scikit-learn</td>
</tr><tr><th scope="row">Bioinformatics</th>
<td>BLAST, Bowtie, Picard, SAMtools, VCFtools</td>
</tr><tr style="background-color:#D3D3D3"><td align="center" colspan="2">Typesetting and Publishing Tools</td>
</tr><tr><th scope="row">Typesetting</th>
<td>TeX Live, Ghostscript, Doxygen</td>
</tr></tbody></table><p>&nbsp;</p>
<h3><a name="Chaining" id="Chaining">4. Chaining Software Modules</a></h3>
<p>Environment Modules also allow a user to optionally integrate their own application environment together with the system-provided application environment, by allowing different categories of modulefiles to be chained together. This provides a common interface for simplicity, while still maintaining diversity and flexibility:</p>
<ul><li>The first category of the modulefiles are provided and maintained by Research IT, which include the commonly used applications and libraries, such as compilers, math libraries, I/O libraries, data processing and visualization tools, etc. We use a hierarchical structure to maintain the cleanness without losing the flexibility of it.<br>
&nbsp;</li>
<li>The second category of the modulefiles are automatically chained for the group of users who belong to the same group on the cluster, if the modulefiles exist in the designated directory. This allows the same group of users to share some of the common applications that they use for collaboration and saves spaces. Normally the user group maintains these modulefiles. But Research&nbsp;IT can also provide assistance under support agreement and on a per request basis.<br>
&nbsp;</li>
<li>The third category of the modulefiles can also be chained on demand by a user if the user chooses to use Environment Modules to manage user specific applications as well. To do that, user needs to append the location of the modulefiles to the environment variable <em>MODULEPATH</em>. This can be done in one of the following ways:1). For bash users, please add the following to ~/.bashrc:<br><pre>export MODULEPATH=$MODULEPATH:/location/to/my/modulefiles</pre><p>
2). For csh/tcsh users, please add the following to ~/.cshrc:</p>
<pre>setenv MODULEPATH "$MODULEPATH":/location/to/my/modulefiles</pre></li>
</ul><h3><a name="Installing" id="Installing">5. Installing Your Own</a></h3>
<p>In addition to the software provided on the cluster, you are welcome to install your own software. Before installing software yourself, first check if it is already provided on the cluster by running <code>module avail</code> and looking to see if the software is listed. Please note that some modules are listed hierarchically, and will only appear on the list after the parent module has been loaded (e.g. libraries for C compilers will only appear after you’ve loaded the respective parent module.)&nbsp;</p>
<h2><strong>Requirements for software on Savio</strong></h2>
<p>Software you install on the cluster will need to:</p>
<ul><li><strong>Be runnable (executable) on Scientific Linux 7</strong> (i.e. essentially <a href="https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/7.0_Release_Notes/index.html">Red Hat Enterprise Linux 7</a>). Choose the x86_64 tarball where available.</li>
<li><strong>Run in command line mode</strong>, or - if remote, graphical access is required - provide such access via X Windows (X11).</li>
<li><strong>Be capable of installation without root/sudo privileges</strong>. This may involve adding command line options to installation commands or changing values in installation-related configuration files, for instance; see your vendor- or community-provided documentation for instructions.</li>
<li><strong>Be capable of installation in the storage space you have available</strong>. (For instance, the source code, intermediate products of installation scripts, and installed binaries must fit within your 10 GB space provided for your home directory, or within a group directory if the software is to be shared with other members of your group.)</li>
<li>If compiled from source, <strong>be capable of being built using the compiler suites on Savio (GCC and Intel)</strong>, or via user-installed compilers.</li>
<li><strong>Be capable of running without a </strong><strong><em>persistent</em></strong><strong> database running on Savio</strong>. An externally hosted database, to which your software on Savio connects, is OK. So is a database that is run on Savio only during execution of your job(s), which is populated by reading files from disk and whose state is saved (if necessary) by exporting the database state to files on disk.</li>
<li>If commercial or otherwise license-restricted, <strong>come with a license that permits cluster usage (multi-core, and potentially multi-node)</strong>, as well as a license enforcement mechanism (if any) that's compatible with the Savio environment.</li>
</ul><p>If your software has installation dependencies – such as libraries, interfaces, or modules – first check whether they are already provided on the cluster before installing them yourself. Make sure that you've first loaded the relevant compiler, interpreted language, or application before examining the list of provided software, because that list is dynamically adjusted based on your current environment.</p>
<p>&nbsp;</p>
<h2><strong>Installation location</strong></h2>
<p>The most important part of installing software on Savio is identifying where you should install it, and how you should modify the installation script to point to the right location.</p>
<p>If you are installing software exclusively for your use, you can install it in your Home directory (/global/home/users/YOUR-USER-NAME). More often, people are installing software for their whole group to use; in that case, you should install it in your group directory (/global/home/groups/YOUR-GROUP-NAME). If your group does not have a shared directory defined, and you need one, please email <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>. <em>In any case, be cognizant of space limitations in your Home directory (10GB) or group directory (see </em><a href="http://research-it.berkeley.edu/services/high-performance-computing/user-guide/savio-user-guide#Storage" class="toc-filter-processed"><em>documentation on storage limits for different types of groups</em></a><em>).</em></p>
<p>If you will be doing a lot of software installation, you may want to add sub-directories for <em>sources </em>(source files downloaded), <em>modules</em> (the installed software), <em>scripts</em> (if you want to document and routinize your installation process using a script -- which is recommended), and <em>modfiles</em> (to create module files that will make software installed in a group directory visible to your group members via the modules command).</p>
<p>&nbsp;</p>
<h2><strong>Example installation process</strong></h2>
<p>The following example illustrates how to install the <a href="http://www.gdal.org/">GDAL geospatial library</a>. It assumes that you have set up sub-directories as discussed above.</p>
<ul><li>Find the URL for the Linux binary tarball for your source code.
<ul><li>For this example, we can find linked to from the <a href="http://www.gdal.org">GDAL website</a>, under Download and then Sources:</li>
<li>Tarball: <a href="http://download.osgeo.org/gdal/2.2.1/gdal-2.2.1.tar.gz">http://download.osgeo.org/gdal/2.2.1/gdal-2.2.1.tar.gz</a></li>
</ul></li>
<li>Change to the directory where you want to install the software (e.g. your Home directory or group directory.) If you have a created <em>sources</em> sub-directory to help keep things tidy, move there; otherwise, you can simply download the source within your installation directory. Example:</li>
</ul><p><code>cd /global/home/groups/my_group/sources</code></p>
<ul><li>Run <code>wget</code> [URL for source tarball].
<ul><li>For this example:</li>
</ul></li>
</ul><p><code>wget http://download.osgeo.org/gdal/2.2.1/gdal-2.2.1.tar.gz</code></p>
<ul><li>Untar the file you downloaded by running <code>tar -zxvf your-file.tar.gz</code>
<ul><li>For the example:</li>
</ul></li>
</ul><p><code>tar -zxvf gdal-2.2.1.tar.gz</code></p>
<ul><li>Change to the new directory that was created with the contents of the tarball.
<ul><li>For the example:</li>
</ul></li>
</ul><p><code>cd gdal-2.2.1</code></p>
<ul><li>Check the documentation for your software to determine where and how you can set the parameters for where the software will be installed. This varies from package to package, and may require modifying the configuration files in the source code itself. Make those changes as needed.
<ul><li>For the gdal example (and this is the case for lots of other software), the documentation indicates that we can specify the installation location by adding --prefix=/put-location-here when running the config file.</li>
</ul></li>
<li>Run the config file, adding in any required parameters for specifying location. If you’ve created a <em>modules</em> subfolder in your target directory, you may want to additionally create a directory for the software package, and a subdirectory for each version. If your software doesn’t have a config file, you will have to modify the Makefile itself to build it. Building the software can be done from any directory where you have the correct permissions. Once you have a binary, you can copy it to the correct location.
<ul><li>For the example:</li>
<li><code>mkdir -p /global/home/groups/my_group/modules/gdal/2.2.1</code></li>
</ul></li>
</ul><p><code>./configure --prefix=/global/home/groups/my_group/modules/gdal/2.2.1</code></p>
<ul><li>Debug the configuration process as needed.
<ul><li>If the configuration fails due to insufficient permissions, then something in the process is probably trying to use a default path. Double-check that you’ve overridden the default paths for every aspect of the configuration process, to ensure that files are written to directories for which you have write permission.</li>
<li>One way to log everything from the configuration process for later debugging is as follows:</li>
</ul></li>
</ul><p><code>script /global/home/groups/my_group/sources/logfile-software-version</code></p>
<p>When you want to stop logging, run exit. All the output will be logged to the file <em>logfile-software-version</em> (e.g., <em>logfile-gdal-2.2.1</em>) in the sources sub-directory. Build and install the software</p>
<ul><li>
<ul><li>Run:</li>
</ul></li>
</ul><p><code>make</code></p>
<ul><li>
<ul><li>Run:</li>
</ul></li>
</ul><p><code>make install</code></p>
<ul><li>
<ul><li>In most cases, we recommend using the default compiler; in SL7, this is GCC 6.3.0. If you have a particular reason to use the Intel compiler, you’ll need to load it first with <code>module load intel</code>, which loads the default version.</li>
</ul></li>
<li>Change the permissions. You’ll want other people in your group to be able to modify and run the software.
<ul><li>To allow the group to modify the software, change the UNIX group of the installed software. For the example:</li>
</ul></li>
</ul><p><code>cd modules; chgrp -R my_group gdal/</code></p>
<ul><li>
<ul><li>Make the software executable. For the example:</li>
</ul></li>
</ul><p><code>chmod -R g+rwX gdal</code></p>
<p>OPTIONAL: Create a modulefile. Adding a modulefile will mean that your software will appear on the list when people with the right set of permissions run module avail. Here is an example of a modulefile for gdal:</p>
<p><code>#%Module1.0<br>
## gdal 2.2.1<br>
## by Lizzy Borden</code></p>
<p><code>proc ModulesHelp { } {<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;puts stderr "loads the environment for gdal 2.2.1"<br>
}</code></p>
<p><code>module-whatis "loads the environment for gdal 2.2.1"</code></p>
<p><code>set GDAL_DIR /global/home/groups/my_group/modules/gdal/2.2.1/<br>
setenv GDAL_DIR $GDAL_DIR<br>
prepend-path PATH $GDAL_DIR/bin<br>
prepend-path LD_LIBRARY_PATH $GDAL_DIR/lib<br>
prepend-path MANPATH $GDAL_DIR/man</code></p>
<p>Name the module file using the version number, in the case of the example “2.2.1”. Place the module file in the modfiles sub-directory and allow access by your group. For the example:</p>
<p><code>mkdir modfiles/gdal<br>
mv 2.2.1 modfiles/gdal<br>
cd modfiles</code></p>
<p><code>chgrp -R my_group gdal</code></p>
<p><code>chmod -R g+rwX gdal</code></p>
<p>&nbsp;</p>
<p>Finally, tell your group members they will need to add <code>/global/home/groups/my_group/modfiles</code> to their <code>MODULEPATH</code> environment variable, which would usually be done in one’s <code>.bashrc</code> file:</p>
<p><code>export MODULEPATH=$MODULEPATH:/global/home/groups/my_group/modfiles</code></p>
<p>&nbsp;</p>
<h2><strong>Example installation scripts</strong></h2>
<p>These examples use tee instead of script to create log files. They also compile the software in parallel with the -j8 flag.</p>
<p>&nbsp;</p>
<h3><strong>gnuplot</strong></h3>
<p>&nbsp;</p>
<p><code>#!/bin/sh<br>
make distclean<br>
./configure --prefix=/global/home/groups/my_group/modules/gnuplot/4.6.0 --with-readline=gnu --with-gd=/usr 2&gt;&amp;1 | tee gnuplot-4.6.0.configure.log<br>
make -j8 2&gt;&amp;1 | tee gnuplot-4.6.0.make.log<br>
make check 2&gt;&amp;1 | tee gnuplot-4.6.0.check.log<br>
make install 2&gt;&amp;1 | tee gnuplot-4.6.0.install.log<br>
make distclean</code></p>
<h3><strong>cgal</strong></h3>
<p><code>#!/bin/sh<br>
module load gcc/4.4.7 openmpi/1.6.5-gcc qt/4.8.0 cmake/2.8.11.2 boost/1.54.0-gcc<br>
make distclean<br>
cmake -DCMAKE_INSTALL_PREFIX=/global/home/groups/my_group/modules/cgal/4.4-gcc . 2&gt;&amp;1 | tee cgal-4.4-gcc.cmake.log<br>
make -j8 2&gt;&amp;1 | tee cgal-4.4-make.log<br>
make install 2&gt;&amp;1 | tee cgal-4.4-install.log<br>
make distclean</code></p>
<h2><strong>FAQ</strong></h2>
<h3><strong>What if the software doesn’t come with a configure script?</strong></h3>
<p>If the software doesn’t come with a configure script, you will have to modify the Makefile itself to build it. Building the software can be done from any directory where you have the correct permissions. Once you have a binary, you can copy it to the correct location.</p>
