---
title: Condo Partner Access
keywords: 
last_updated: October 1, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/condo-partner-access
folder: hpc
---

<p>By becoming a Condo Partner, through purchasing and contributing compute nodes to the Savio cluster via the <a href="http://research-it.berkeley.edu/services/high-performance-computing/condo-cluster-service">Condo Cluster Service</a>, researchers and their groups obtain priority - and hence nearly unlimited - access to resources equivalent to their contribution.</p>
<p>In addition, Condo partners can access any of the other resources in the cluster by using the no-cost <a href="http://research-it.berkeley.edu/blog/16/01/29/low-priority-queue-allows-condo-contributors-glean-unused-cycles-savio">low priority queue</a>. (Such jobs are subject to preemption; for details, please see the "Low Priority Jobs" section of the <a href="http://research-it.berkeley.edu/services/high-performance-computing/user-guide">User Guide</a>.)</p>
<p><strong>Accessing Savio for Condo partners</strong></p>
<p>For information on logging into Savio, obtaining additional user accounts, and closing these accounts, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/getting-account">Getting an Account</a>.</p>
<p>Important details for Condo partners on how to submit jobs to the cluster (such as the names of scheduler queues and scheduler policies for each Condo) can be found in the <a href="http://research-it.berkeley.edu/services/high-performance-computing/user-guide#Scheduler" class="toc-filter-processed">Scheduler Configuration</a> section of the User Guide.</p>
