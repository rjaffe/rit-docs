---
title: Cortex User Guide
keywords: high performance computing, cortex
last_updated: October 17, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/cortex-user-guide
folder: hpc
---

<p>This is the User Guide for users of Cortex, a Neurosciences high-performance computing cluster administered by <a href="http://research-it.berkeley.edu/programs/berkeley-research-computing">Berkeley Research Computing</a> at the University of California, Berkeley.</p>
<h4 id="Account-Requests">Account Requests</h4>
<p>You can request new accounts on the Cortex cluster via this <a href="https://docs.google.com/forms/d/e/1FAIpQLSeE8GLx-qM3mO9t0liZXBQ_MEY7aLxi0hOsDIMPz2Y9AtGudw/viewform">online form</a>.</p>
<h4 id="Logging-In">Logging in</h4>
<p>Cortex uses One Time Passwords (OTPs) for login authentication. For details, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/logging-brc-clusters">Logging in</a>.</p>
<p>Use SSH to log into:&nbsp; <code>hpc.brc.berkeley.edu</code></p>
<h4 id="Transferring-Data">Transferring Data</h4>
<p>To transfer data to and from Cortex, use the cluster's dedicated Data Transfer Node (DTN):&nbsp; <code>dtn.brc.berkeley.edu</code></p>
<p>If you're using <a href="http://research-it.berkeley.edu/services/high-performance-computing/using-globus-connect-savio">Globus</a> to transfer files, the Globus endpoint is:&nbsp; <code>ucb#brc</code></p>
<p>For details about how to transfer files to and from the cluster, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/transferring-data">Transferring Data</a>.</p>
<h4>Storage and Backup</h4>
<p>The following storage systems are available to Cortex cluster users:</p>
<table align="center" border="1" cellspacing="0"><tbody><tr style="background-color: rgb(211, 211, 211);"><th>Name</th>
<th>Location</th>
<th>Quota</th>
<th>Backup</th>
<th>Allocation</th>
<th>Description</th>
</tr><tr><td>HOME</td>
<td><code>/global/home/users/$USER</code></td>
<td>10GB</td>
<td>Yes</td>
<td>Per User</td>
<td>Home directory ($HOME) for permanent data</td>
</tr><tr><td>CLUSTERFS</td>
<td><code>/clusterfs/cortex/users/$USER</code></td>
<td>none</td>
<td>No</td>
<td>Per User</td>
<td>Private storage</td>
</tr></tbody></table><h4 id="Hardware-Configuration">Hardware Configuration</h4>
<p>Cortex is a heterogeneous cluster, with a mix of three different types of nodes, each with different hardware configurations. Two of these configurations offer Graphics Processing Units (GPUs) in addition to CPUs:</p>
<table align="center" border="1"><tbody><tr style="background-color:#D3D3D3"><th>Partition</th>
<th>Nodes</th>
<th>Node List</th>
<th>CPU</th>
<th>Cores</th>
<th>Memory</th>
<th>GPU</th>
</tr><tr><td rowspan="3">cortex</td>
<td rowspan="3">14</td>
<td>n00[00-01].cortex0</td>
<td>INTEL Xeon E5410</td>
<td>8</td>
<td>16GB</td>
<td>1x Tesla K40</td>
</tr><tr><td>n00[02-11].cortex0</td>
<td>INTEL Xeon E3-1220</td>
<td>4</td>
<td>16GB</td>
<td>&nbsp;</td>
</tr><tr><td>n00[12-13].cortex0</td>
<td>INTEL Xeon X5650</td>
<td>12</td>
<td>24GB</td>
<td>2x Tesla M2050</td>
</tr></tbody></table><p>Please be aware of these various hardware configurations, along with their associated <a href="#Scheduler-Configuration" class="toc-filter-processed">scheduler configurations</a>, when specifying options for running your jobs.</p>
<h4 id="Scheduler-Configuration">Scheduler Configuration</h4>
<p>The Cortex cluster uses the SLURM scheduler to manage jobs. For many examples of job script files that you can adapt and use for running your own jobs, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/running-your-jobs">Running Your Jobs</a>.</p>
<p>When submitting your jobs via SLURM's <code>sbatch</code> or <code>srun</code> commands, use the following options:</p>
<ul><li>The "cortex" partition: <code>--partition=cortex</code></li>
<li>The "cortex" account: <code>--account=cortex</code></li>
<li>Optionally, a node feature, as shown in&nbsp; the "Node Features" column in the table below. For example, to select only nodes providing a Tesla K40 GPU: <code>--constraint=cortex_k40</code> If the node feature ("<code>--constraint</code>") option is <em>not</em> used, the default order of dispatch to nodes will be to <code>n00[02-11].cortex0</code>, then to <code>n00[00-01].cortex0</code>, and finally to <code>n00[12-13].cortex0</code>.</li>
<li>The "cortex_c32" QoS will be applied by default. Thus no QoS option is required when using the Cortex resources.<br>
&nbsp;</li>
</ul><table align="center" border="1"><tbody><tr style="background-color:#D3D3D3"><th>Partition</th>
<th>Account</th>
<th>Nodes</th>
<th>Node List</th>
<th>Node Feature</th>
<th>Shared</th>
<th>QoS</th>
<th>QoS Limit</th>
</tr><tr><td rowspan="3">cortex</td>
<td rowspan="3">cortex</td>
<td rowspan="3">14</td>
<td>n00[00-01].cortex0</td>
<td>cortex, cortex_k40</td>
<td rowspan="3">cortex_c32</td>
<td rowspan="3">Yes</td>
<td rowspan="3">No Limit</td>
</tr><tr><td>n00[02-11].cortex0</td>
<td>cortex, cortex_nogpu</td>
</tr><tr><td>n00[12-13].cortex0</td>
<td>cortex, cortex_fermi</td>
</tr></tbody></table><p>To help ensure fair access to cluster resources for all of its users, a standard fair-share policy, with a decay half life value of 14 days (2 weeks), is enforced.</p>
<h4><span style="font-size: 1.2em;">Software Configuration</span></h4>
<p>For details about how to find and access the software provided on the cluster, as well as on how to install your own, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/accessing-and-installing-software">Accessing and Installing Software</a>.</p>
<h4 id="Cluster-Status">Cluster Status</h4>
<p>The live status of the CORTEX cluster is <a href="http://metacluster.lbl.gov/warewulf/cortex">displayed here</a>.</p>
<h4 id="Getting-Help">Getting Help</h4>
<p>For inquiries or service requests, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/getting-help">BRC's Getting Help page</a> or send email to <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>.</p>

