---
title: Savio User Guide
keywords: high performance computing
last_updated: October 17, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/user-guide/savio-user-guide
folder: hpc
---

<p>This&nbsp;<a href="https://www.youtube.com/playlist?list=PLinUqTXTvciOPBdqi9gXgAuRX7Gew2bLd">Intro to Savio video&nbsp;playlist on YouTube</a> covers the basics for getting started with Savio.&nbsp; Summary info page is available at <a href="http://research-it.berkeley.edu/services/high-performance-computing/system-overview">System-Overview</a>&nbsp;.</p>
<h3><a name="Login" id="Login">Login Procedure</a></h3>
<p>The Savio cluster uses One Time Passwords (OTP) for login authentication. You will need to download and configure the Google Authenticator&nbsp;application on a tablet, and/or smart phone (Android or iOS)&nbsp;to generate these one time passwords. Please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/logging-savio">Logging into Savio </a>for instructions on installing and configuring Google Authenticator.</p>
<ul class="rteindent1"><li>Login Protocol: ssh</li>
<li>Login Server: hpc.brc.berkeley.edu</li>
</ul><h3><a name="Data_Transfer" id="Data_Transfer">Data Transfer</a></h3>
<p>To transfer data to or from the Savio cluster, connect to the cluster's Data Transfer Node. For instructions, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/transferring-data">Transferring Data</a>.</p>
<ul class="rteindent1"><li>Data Transfer Node: dtn.brc.berkeley.edu</li>
<li>Globus Endpoint Name: ucb#brc</li>
</ul><h3><a name="Storage" id="Storage">Storage and Backup</a></h3>
<p>Savio cluster users are entitled to access the following storage systems. Please be sure to use these filesystems correctly. Users should not use their HOME directory for heavy I/O activity during job runs.</p>
<table align="center" border="1"><thead><tr><th style="text-align: center;">Name</th>
<th style="text-align: center;">Location</th>
<th style="text-align: center;">Quota</th>
<th style="text-align: center;">Backup</th>
<th style="text-align: center;">Allocation</th>
<th style="text-align: center;">Description</th>
</tr></thead><tbody><tr><td>HOME</td>
<td>/global/home/users/</td>
<td>10 GB</td>
<td>Yes</td>
<td>Per User</td>
<td>HOME directory for permanent data</td>
</tr><tr><td>GROUP</td>
<td>/global/home/groups/</td>
<td>30/200 GB</td>
<td>No</td>
<td>Per Group</td>
<td>GROUP directory for shared data (30 GB for FCA, 200 GB for Condo)</td>
</tr><tr><td>SCRATCH</td>
<td>/global/scratch/</td>
<td>none</td>
<td>No</td>
<td>Per User</td>
<td>SCRATCH directory with Lustre FS. Ongoing purge policy to delete anyfile not accessed in 6 months</td>
</tr></tbody></table><p>For information on making your files accessible to other users (in particular members of your group), see <a href="https://research-it.berkeley.edu/services/high-performance-computing/transferring-data/making-files-accessible-group-members-and">these instructions</a>.</p>
<h3><a name="Hardware" id="Hardware">Hardware Configuration</a></h3>
<p>Please refer to the following table with the hardware configuration of each generation of nodes. A high performance Lustre file system is also available as scratch space to all users.</p>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:center">Partition</th>
<th style="text-align:center">Nodes</th>
<th style="text-align:center">Node List</th>
<th style="text-align:center">CPU Model</th>
<th style="text-align:center"># Cores/Node</th>
<th style="text-align:center">Memory/Node</th>
<th style="text-align:center">Infiniband</th>
<th style="text-align:center">Speciality</th>
<th style="text-align:center">Scheduler Allocation</th>
</tr><tr><td style="text-align:center; vertical-align:middle">savio</td>
<td style="text-align:center; vertical-align:middle">164</td>
<td style="text-align:center; vertical-align:middle">n0[000-095].savio1<br>
n0[100-167].savio1</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v2</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio_bigmem</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[096-099].savio1</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v2</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">512 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">136</td>
<td style="text-align:center; vertical-align:middle">n0[027-162].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[183-186].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2680 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[290-293].savio2<br>
n0[230-240].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2650 v4</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">35</td>
<td style="text-align:center; vertical-align:middle">n0[187-210].savio2<br>
n0[230-240].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2680 v4</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_bigmem</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">n0[163-182].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_bigmem</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">n0[282-289].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2650 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_htc</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">n0[000-011].savio2<br>
n0[215-222].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2643 v3</td>
<td style="text-align:center; vertical-align:middle">12</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">HTC</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_gpu</td>
<td style="text-align:center; vertical-align:middle">17</td>
<td style="text-align:center; vertical-align:middle">n0[012-026].savio2<br>
n0[223-224].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2623 v3</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x Nvidia K80</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_1080ti</td>
<td style="text-align:center; vertical-align:middle">3</td>
<td style="text-align:center; vertical-align:middle">n0[227-229].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2623 v3</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x Nvidia 1080ti</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_knl</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">n0[254-281].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Phi 7210</td>
<td style="text-align:center; vertical-align:middle">64</td>
<td style="text-align:center; vertical-align:middle">188 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">Intel Phi</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">16</td>
<td style="text-align:center; vertical-align:middle">n00[10-25].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">&nbsp;</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_bigmem</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n00[06-09].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">384 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">&nbsp;</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_xlmem</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">n0000.savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">1.5 TB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">XL Memory</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">n0001.savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">2x Tesla V100 GPU</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr></tbody></table><h3><a name="Scheduler" id="Scheduler">Scheduler Configuration</a></h3>
<p>The Savio cluster uses <a href="http://research-it.berkeley.edu/services/high-performance-computing/running-your-jobs">SLURM</a> as the scheduler to manage jobs on the cluster. Please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/running-your-jobs">Running Your Jobs</a> for instructions on using the scheduler, as well as taking note of crucial additional details, below.</p>
<p>Three different models are supported when running jobs on the Savio cluster through the scheduler:</p>
<ul class="rteindent1"><li>Condo model - Faculty and principal investigators can join the <a href="http://research-it.berkeley.edu/services/high-performance-computing/condo-cluster-service" style="line-height: 1.6;">Condo program</a> by purchasing compute nodes which are contributed to the cluster. Users from the condo group are granted ongoing, no cost use of compute resources for running their jobs, up to the amount contributed. Condo users can also access larger amounts of compute resources at no cost via Savio's preemptable, <a href="#Low_Priority" class="toc-filter-processed">low-priority quality of service option</a>.</li>
<li>Faculty Computing Allowance (FCA) model - This program provides qualified faculty and principal investigators with up to 300K <a href="http://research-it.berkeley.edu/services/high-performance-computing/service-units-savio">Service Units</a> on the Savio cluster at no cost, during each annual application period.&nbsp;(Allowances are prorated based on month of application during the allowance year.) Please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/faculty-computing-allowance">Faculty Computing Allowance</a> for more details.</li>
<li>Memorandum of Understanding (MOU) model&nbsp;- If you use your entire Faculty Computing Allowance for a year and need additional compute time on Savio, please contact us at <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>. We can help you set up an MOU, through which you can pay for additional blocks of compute time at a rate that is highly competitive with those of other compute providers.</li>
</ul><p>Depending on the specific group(s) to which a user belongs, s/he may have access to one or more of the models described above. Therefore, it is highly recommended that all users become familiar with the following scheduler configuration on the Savio cluster to use its resources in an efficient manner:</p>
<ul class="rteindent1"><li>The partition name “savio” or “savio_bigmem” (“--partition=savio” or “--partition=savio_bigmem”) is needed in all cases.</li>
<li>The designated account that was assigned to the user is needed in all cases (<em>e.g.</em>, --account=<strong>fc_{PUT_YOUR_ACCOUNT_HERE}</strong>).</li>
<li>The appropriate Quality of Service (QoS) selection is also required (but in most cases you do not need to specify this and can rely on the default QoS); the specific QoS which applies to each user will depend on his/her project(s) and the model under which the job should run:<br><ul class="rteindent2"><li>To run jobs with the condo model, the proper condo QoS (<em>e.g.</em>, --qos=<strong>{PUT_YOUR_CONDO_NAME_HERE}</strong>_normal) should be used.</li>
<li>To run jobs with the FCA model, the proper FCA QoS (<em>e.g.</em>, --qos=<strong>savio</strong>_normal) should be used.</li>
<li>To run jobs with the MOU&nbsp;model, the proper MOU&nbsp;QoS (<em>e.g.</em>, --qos=<strong>savio</strong>_normal, or --qos=<strong>savio</strong>_debug) should be used.</li>
</ul></li>
<li>A standard fair-share policy with a decay half-life of 14 days (2 weeks) is enforced.</li>
</ul><h5>Configuration Details</h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:center">Partition</th>
<th style="text-align:center">Nodes</th>
<th style="text-align:center">Node<br>
Features</th>
<th style="text-align:center">Shared</th>
<th style="text-align:center"><a href="http://research-it.berkeley.edu/services/high-performance-computing/service-units-savio">SU/CORE Hour<br>
Ratio</a></th>
<th style="text-align:center">Account</th>
<th style="text-align:center">QoS</th>
<th style="text-align:center">QoS Limit</th>
</tr><tr><td style="text-align:center; vertical-align:middle">savio</td>
<td style="text-align:center; vertical-align:middle">164</td>
<td style="text-align:center; vertical-align:middle">savio</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">0.75</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:85px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:20px; vertical-align:middle"><a href="#Savio_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:20px; vertical-align:middle; width:170px"><a href="#Savio_Condo" class="toc-filter-processed">Savio Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio_<br>
bigmem</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">savio_<br>
bigmem<br>
or<br>
savio_<br>
m512</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">1.67</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio_Bigmem_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio_Bigmem_Condo" class="toc-filter-processed">Savio Bigmem Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">179</td>
<td style="text-align:center; vertical-align:middle">savio2<br>
or<br>
savio2_<br>
c24<br>
or<br>
savio2_<br>
c28</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">1.00</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:85px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:20px; vertical-align:middle"><a href="#Savio2_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:20px; vertical-align:middle; width:170px"><a href="#Savio2_Condo" class="toc-filter-processed">Savio2 Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br>
bigmem</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">savio2_<br>
bigmem<br>
or<br>
savio2_<br>
m128</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">1.20</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_Bigmem_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_Bigmem_Condo" class="toc-filter-processed">Savio2 Bigmem Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br>
htc</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">savio2_<br>
htc</td>
<td style="text-align:center; vertical-align:middle">Shared</td>
<td style="text-align:center; vertical-align:middle">1.20</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:213px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:72px; vertical-align:middle">savio_long</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_HTC_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">4 cores max per job<br>
24 cores in total<br>
10 day wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_HTC_Condo" class="toc-filter-processed">Savio2 HTC Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br>
gpu</td>
<td style="text-align:center; vertical-align:middle">17</td>
<td style="text-align:center; vertical-align:middle">savio2_<br>
gpu</td>
<td style="text-align:center; vertical-align:middle">Shared</td>
<td style="text-align:center; vertical-align:middle">2.67</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_GPU_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_GPU_Condo" class="toc-filter-processed">Savio2 GPU Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br>
1080ti</td>
<td style="text-align:center; vertical-align:middle">3</td>
<td style="text-align:center; vertical-align:middle">savio2_<br>
1080ti</td>
<td style="text-align:center; vertical-align:middle">Shared</td>
<td style="text-align:center; vertical-align:middle">1.67</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_1080ti_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_1080ti_Condo" class="toc-filter-processed">Savio2 1080ti Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_<br>
knl</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">savio2_<br>
knl</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">0.40</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio2_KNL_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio2_KNL_Condo" class="toc-filter-processed">Savio2 KNL Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">16</td>
<td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">TBD</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio3_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio3_Condo" class="toc-filter-processed">Savio3 Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_bigmem</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">savio3_bigmem</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">TBD</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio3_Bigmem_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio3_Bigmem_Condo" class="toc-filter-processed">Savio3 Bigmem Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_xlmem</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">savio3_xlmem</td>
<td style="text-align:center; vertical-align:middle">Exclusive</td>
<td style="text-align:center; vertical-align:middle">TBD</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio3_Xlmem_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio3_Xlmem_Condo" class="toc-filter-processed">Savio3 Xlmem Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">Shared</td>
<td style="text-align:center; vertical-align:middle">TBD</td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:133px; vertical-align:middle">ac_*<br>
fc_*<br>
pc_*<br>
ic_*</td>
</tr><tr><td style="height:111px; vertical-align:middle">co_*</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle">savio_debug</td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_normal</td>
</tr><tr><td style="height:50px; vertical-align:middle"><a href="#Savio3_GPU_Condo" class="toc-filter-processed">Condo QoS</a></td>
</tr><tr><td style="height:50px; vertical-align:middle">savio_lowprio</td>
</tr></tbody></table></td>
<td style="text-align:center; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px; vertical-align:middle; width:170px">4 nodes max per job<br>
4 nodes in total<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px"><a href="#Savio3_GPU_Condo" class="toc-filter-processed">Savio3 GPU Condo QoS Conf</a></td>
</tr><tr><td style="height:50px; vertical-align:middle; width:170px">24 nodes max per job<br>
72:00:00 wallclock limit</td>
</tr></tbody></table></td>
</tr></tbody></table><p><strong>NOTE:</strong> To check which account and/or QoS that a user is allowed to use, please run "sacctmgr -p show associations user=$USER".</p>
<h5><a name="Savio_Condo" id="Savio_Condo">Savio Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_acrb</td>
<td style="text-align:left; vertical-align:middle">acrb_savio_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_aiolos</td>
<td style="text-align:left; vertical-align:middle">aiolos_savio_normal</td>
<td style="text-align:left; vertical-align:middle">12 nodes max per group<br>
24:00:00 wallclock limit</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_astro</td>
<td style="text-align:left; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px">astro_savio_debug</td>
</tr><tr><td style="height:50px">astro_savio_normal</td>
</tr></tbody></table></td>
<td style="text-align:left; vertical-align:middle">
<table border="1" cellspacing="0" style="border-collapse:collapse; border-color:rgb(136,136,136); border-width:1px"><tbody><tr><td style="height:72px">4 nodes max per group<br>
4 nodes max per job<br>
00:30:00 wallclock limit</td>
</tr><tr><td style="height:50px">32 nodes max per group<br>
16 nodes max per job</td>
</tr></tbody></table></td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_dlab</td>
<td style="text-align:left; vertical-align:middle">dlab_savio_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_nuclear</td>
<td style="text-align:left; vertical-align:middle">nuclear_savio_normal</td>
<td style="text-align:left; vertical-align:middle">24 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_praxis</td>
<td style="text-align:left; vertical-align:middle">praxis_savio_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_rosalind</td>
<td style="text-align:left; vertical-align:middle">rosalind_savio_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group<br>
4 nodes max per job per user</td>
</tr></tbody></table><h5><a name="Savio_Bigmem_Condo" id="Savio_Bigmem_Condo">Savio Bigmem Condo QoS Configurations</a></h5>
<p>&nbsp;</p>
<h5><a name="Savio2_Condo" id="Savio2_Condo">Savio2 Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_biostat</td>
<td style="text-align:left; vertical-align:middle">biostat_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">20 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_chemqmc</td>
<td style="text-align:left; vertical-align:middle">chemqmc_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">16 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_dweisz</td>
<td style="text-align:left; vertical-align:middle">dweisz_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_econ</td>
<td style="text-align:left; vertical-align:middle">econ_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">2 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_hiawatha</td>
<td style="text-align:left; vertical-align:middle">hiawatha_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">40 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_lihep</td>
<td style="text-align:left; vertical-align:middle">lihep_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_mrirlab</td>
<td style="text-align:left; vertical-align:middle">mrirlab_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_planets</td>
<td style="text-align:left; vertical-align:middle">planets_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_stat</td>
<td style="text-align:left; vertical-align:middle">stat_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">2 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_bachtrog</td>
<td style="text-align:left; vertical-align:middle">bachtrog_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_noneq</td>
<td style="text-align:left; vertical-align:middle">noneq_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_kranthi</td>
<td style="text-align:left; vertical-align:middle">kranthi_savio2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio2_Bigmem_Condo" id="Savio2_Bigmem_Condo">Savio2 Bigmem Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_laika</td>
<td style="text-align:left; vertical-align:middle">laika_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_dweisz</td>
<td style="text-align:left; vertical-align:middle">dweisz_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_aiolos</td>
<td style="text-align:left; vertical-align:middle">aiolos_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group<br>
24:00:00 wallclock limit</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_bachtrog</td>
<td style="text-align:left; vertical-align:middle">bachtrog_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">4 nodes max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_msedcc</td>
<td style="text-align:left; vertical-align:middle">msedcc_bigmem2_normal</td>
<td style="text-align:left; vertical-align:middle">8 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio2_HTC_Condo" id="Savio2_HTC_Condo">Savio2 HTC Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_rosalind</td>
<td style="vertical-align: middle;">rosalind_htc2_normal</td>
<td style="vertical-align: middle;">8 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio2_GPU_Condo" id="Savio2_GPU_Condo">Savio2 GPU Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="text-align:left; vertical-align:middle">co_acrb</td>
<td style="text-align:left; vertical-align:middle">acrb_gpu2_normal</td>
<td style="text-align:left; vertical-align:middle">44 GPUs max per group</td>
</tr><tr><td style="text-align:left; vertical-align:middle">co_stat</td>
<td style="text-align:left; vertical-align:middle">stat_gpu2_normal</td>
<td style="text-align:left; vertical-align:middle">8 GPUs max per group</td>
</tr></tbody></table><h5><a name="Savio2_1080ti_Condo" id="Savio2_1080ti_Condo">Savio2 1080ti Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_acrb</td>
<td style="vertical-align: middle;">acrb_1080ti2_normal</td>
<td style="vertical-align: middle;">12 GPUs max per group</td>
</tr><tr><td style="vertical-align: middle;">co_mlab</td>
<td style="vertical-align: middle;">mlab_1080ti2_normal</td>
<td style="vertical-align: middle;">16 GPUs max per group</td>
</tr></tbody></table><h5><a name="Savio2_KNL_Condo" id="Savio2_KNL_Condo">Savio2 KNL Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_lsdi</td>
<td style="vertical-align: middle;">lsdi_knl2_normal</td>
<td style="vertical-align: middle;">28 nodes max per group<br>
5 running jobs max per user<br>
20 total jobs max per user</td>
</tr></tbody></table><h5><a name="Savio3_Condo" id="Savio3_Condo">Savio3 Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_chemqmc</td>
<td style="vertical-align: middle;">chemqmc_savio3_normal</td>
<td style="vertical-align: middle;">4 nodes max per group</td>
</tr><tr><td style="vertical-align: middle;">co_laika</td>
<td style="vertical-align: middle;">laika_savio3_normal</td>
<td style="vertical-align: middle;">4 nodes max per group</td>
</tr><tr><td style="vertical-align: middle;">co_noneq</td>
<td style="vertical-align: middle;">noneq_savio3_normal</td>
<td style="vertical-align: middle;">8 nodes max per group</td>
</tr><tr><td style="vertical-align: middle;">co_aiolos</td>
<td style="vertical-align: middle;">aiolos_savio3_normal</td>
<td style="vertical-align: middle;">36 nodes max per group<br>
24:00:00 wallclock limit</td>
</tr></tbody></table><h5><a name="Savio3_Bigmem_Condo" id="Savio3_Bigmem_Condo">Savio3 Bigmem Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_genomicdata</td>
<td style="vertical-align: middle;">genomicdata_bigmem3_normal</td>
<td style="vertical-align: middle;">4 nodes max per group</td>
</tr></tbody></table><h5><a name="Savio3_Xlmem_Condo" id="Savio3_Xlmem_Condo">Savio3 Xlmem Condo QoS Configurations</a></h5>
<table align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:left">Account</th>
<th style="text-align:left">QoS</th>
<th style="text-align:left">QoS Limit</th>
</tr><tr><td style="vertical-align: middle;">co_genomicdata</td>
<td style="vertical-align: middle;">genomicdata_xlmem3_normal</td>
<td style="vertical-align: middle;">1 nodes max per group</td>
</tr></tbody></table><h5><a name="Low_Priority" id="Low_Priority">Low Priority Jobs</a></h5>
<p>All condo contributors (account name starts with "co_") are entitled to use the extra resource that is available on the Savio cluster (across all partitions). The is done through a low priority QoS "savio_lowprio" and your account is automatically subscribed to this QoS during the account creation stage. You do not need to request for it explicitly. By using this QoS you are no longer limited by your condo size. What this means to users is that you will now have access to the broader compute resource which is limited by the size of partitions. However this QoS does not get a priority as high as the general QoSs, such as "savio_normal" and "savio_debug", or all the condo QoSs, and it is subject to preemption when all the other QoSs become busy. Thus it has two implications:</p>
<ol><li>When system is busy, any job that is submitted with this QoS will be pending and yield to other jobs with higher priorities.</li>
<li>When system is busy and there are higher priority jobs pending, scheduler will preempt jobs that are running with this lower priority QoS. Preempted jobs can choose whether the job should be simply killed, or be automatically requeued after it's killed, at submission time. Please note that, since preemption could happen at any time, it would be very beneficial if your job is capable of checkpointing/restarting by itself, when you choose to requeue the job. Otherwise, you may need to verify data integrity manually before you want to run the job again.</li>
</ol><h5><a name="Examples" id="Examples">Job Script Examples</a></h5>
<p>For many examples of job script files that you can adapt and use for running your own jobs, please see <a href="http://research-it.berkeley.edu/services/high-performance-computing/running-your-jobs">Running Your Jobs</a>.</p>
<h3><a name="Software" id="Software">Software Configuration</a></h3>
<p>The Savio cluster uses <a href="http://research-it.berkeley.edu/services/high-performance-computing/accessing-and-installing-software">Environment Modules</a> to manage the cluster-wide software installation.</p>
