---
title: How to use this site
sidebar: hpc_sidebar
permalink: index.html
tags: [getting_started_rit, creating_content, getting_started]
keywords: hpc
summary: A brief introduction to using this HPC documentation site.
search: exclude
---

## Overview

This site is generated using <a href="https://jekyllrb.com/" target="_blank">Jekyll</a> and was forked from the <a href="https://github.com/tomjoht/documentation-theme-jekyll" target="_blank">Jekyll Documentation Theme</a>.

## Adding Pages and Posts

### Pages

To create a new page, add a new `.md` file somewhere in the `pages` directory. While the directory structure has no effect on URLs, it is good practice to keep the pages organized in subdirectories that correspond to the permalink structure and to name files so that they match the final part of the URL slug.

The page title, summary, tags, and everything below the front matter (see below) will make up the visible content of the page. Any `<h*>` tags that are used (e.g. the markdown for an `<h2>` tag is `##`) will become part of the Table of Contents at the top of the page below the title.

<a href="https://daringfireball.net/projects/markdown/" target="_blank">See here</a> for more info on writing markdown.

### Front Matter

All pages and posts require a preamble. For example, the front matter for this page is

```
---
title: How to use this site
sidebar: hpc_sidebar
permalink: index.html
tags: [getting_started, creating_content]
keywords: hpc
summary: A brief introduction to using this HPC documentation site.
search: exclude
---
```
Note that the homepage gets the special permalink `index.html`, while other pages have the permalinks you would expect. For example, the permalink for [Getting an Account]({{ site.baseurl }}/services/high-performance-computing/getting-account) is `services/high-performance-computing/getting-account`.

Another difference is that this page is not included in search results.

**Required elements**:
* `title` - this will appear in a `<h1>` tag at the top of the page/post and is used by the "search..." field in the header of the site.
* `sidebar` - use `hpc_sidebar`.
* `permalink` - this determines the URL slug (everything after the first `/`). For pages, the final part should match the name of the file (excluding `.md`). It is also used by search.

**Optional elements**:
* `tags` - Appear at the bottom of pages and posts (see below). Also, used by search and in invisible `<meta>` tags which are useful for SEO purposes. Tags should be in brackets and comma-separated as in the example above.
* `keywords` - Used by search and in `<meta>` tags.
* `summary` - Appears beneath the title (as above) and used by search.
* `search` - Use `exclude` if you do not want the page/post to appear in search results.

For more info on the front matter, <a href="https://jekyllrb.com/docs/front-matter/" target="_blank">see here</a>.

### Links within page content

For external links, please use the following HTML snippet so that they open in a new browser tab:

```
<a href="https://your-link.com" target="_blank">This link opens in a new tab.</a>
```

For internal links, `.md` is fine but we need to use a bit of <a href="https://github.com/Shopify/liquid/wiki" target="_blank">Liquid</a> to ensure that the link works the same way in all environments (i.e. local, testing and production). For example, the link above to the Getting an Account page was created using:
```
[Getting an Account]({{ site.baseurl }}/services/high-performance-computing/getting-account)
```
The key here is the small bit of Liquid templating `{{ site.baseurl }}` which prepends the correct URL for a given environment.
